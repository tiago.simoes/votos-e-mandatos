from datetime import datetime
from flask import Flask, render_template, Markup, url_for, redirect
from loader import assert_json, gen_json, load_json
from processor import clean_strings
from pages import results_table, dist_table, compressed_table, analysis_single, analysis_sum, methods_sim, methods_sum
from components import df_info_abbr, df_to_table, menu_html, d_menu_html, k_menu_html, stats_html, stats_card
from generator import print_static
import europeias
import legislativas

app = Flask(__name__)

# Static Site Generator
recorder = True

@app.route("/")
def front():
    output = render_template('index.html', menu_parent='index')
    if recorder:
        print_static(output, [''])
    return output

##
# Eleições europeias
##

@app.route("/europeias")
def europeias_list():

    dict = europeias.init()

    # Navegação vertical
    crumbs = europeias.crumbs('', 'Europeias', levels=['list'])
    breadcrumb = Markup(render_template('components/breadcrumb.html', items=crumbs, active_page='europeias_list'))

    # Conteúdo da página
    markup = Markup(render_template('content/europeias.html'))

    # Barra lateral
    sidebar_markup = Markup(menu_html('Resultados disponíveis', dict['files'], dict['route']))

    output = render_template('sidebar-right.html', title='Eleições europeias', menu_parent='europeias_list', breadcrumb_html=breadcrumb, content_html=markup, sidebar_html=sidebar_markup)
    if recorder:
        print_static(output, ['europeias'])
    return output

# Primeiro nível de navegação

@app.route("/europeias/<key>") # resultados
def europeias_view(key):

    start_time = datetime.now()

    dict = europeias.init()

    if key in dict['files']:
        page_title = dict['files'][key]['title']

        # Carregar os dados
        status, json_path = assert_json(dict['basedir'], dict['files'][key]['csv'])
        if status == False:
            processed_data = europeias.process(dict['basedir'], dict['files'][key]['csv'])
            gen_json(json_path, processed_data)
            status = True
        retrieved_data = load_json(json_path)

        # Navegação vertical
        crumbs = europeias.crumbs(key, page_title, levels=['list','view'])
        breadcrumb = Markup(render_template('components/breadcrumb.html', items=crumbs, active_page='europeias_view'))
        breadcrumb += Markup(menu_html('Eleições', dict['files'], 'europeias_view', component='dropdown'))

        # Navegação horizontal
        tabs = europeias.tabs(key)
        page_nav = Markup(render_template('components/nav-tabs.html', items=tabs, active_tab='europeias_view'))

        # Conteúdo da página
        csv_title = retrieved_data['source']['title']
        df_v = retrieved_data['total']['df_v']
        df_i = retrieved_data['total']['df_i']
        stats = retrieved_data['total']['stats']
        dict_meta = retrieved_data['meta']
        markup = Markup(results_table(csv_title, df_v, 'h', df_i, stats, dict_meta))

        # Fundo da página
        bottom_markup = Markup(stats_html(stats))

        print('\nWEBAPP: página gerada em ' + str((datetime.now() - start_time).total_seconds()) + ' segundos.\n')
        output = render_template('no-sidebar.html', title=page_title, menu_parent='europeias_list', breadcrumb_html=breadcrumb, tabs_html=page_nav, main_html=markup, bottom_html=bottom_markup)
        if recorder:
            print_static(output, ['europeias', key])
        return output

    else:
        return redirect(url_for('europeias_list'), code=302)

@app.route("/europeias/<key>/analise")
def europeias_analysis(key):

    start_time = datetime.now()

    dict = europeias.init()

    if key in dict['files']:
        page_title = dict['files'][key]['title']

        # Carregar os dados
        status, json_path = assert_json(dict['basedir'], dict['files'][key]['csv'])
        if status == False:
            processed_data = europeias.process(dict['basedir'], dict['files'][key]['csv'])
            gen_json(json_path, processed_data)
            status = True
        retrieved_data = load_json(json_path)

        # Navegação vertical
        crumbs = europeias.crumbs(key, page_title, levels=['list','view','analysis'])
        breadcrumb = Markup(render_template('components/breadcrumb.html', items=crumbs, active_page='europeias_analysis'))
        breadcrumb += Markup(menu_html('Eleições', dict['files'], 'europeias_analysis', component='dropdown'))

        # Navegação horizontal
        tabs = europeias.tabs(key)
        page_nav = Markup(render_template('components/nav-tabs.html', items=tabs, active_tab='europeias_analysis'))

        # Conteúdo da página
        df_v = retrieved_data['total']['df_v']
        df_i = retrieved_data['total']['df_i']
        stats = retrieved_data['total']['stats']
        seq_alloc = retrieved_data['total']['alloc']['hondt']['seq']
        dict_meta = retrieved_data['meta']

        markup = Markup(analysis_single(df_v, 'h', stats, seq_alloc, dict_meta))

        # Fundo da página
        bottom_markup = Markup(stats_html(stats))

        print('\nWEBAPP: página gerada em ' + str((datetime.now() - start_time).total_seconds()) + ' segundos.\n')
        output= render_template('no-sidebar.html', title=page_title, menu_parent='europeias_list', breadcrumb_html=breadcrumb, tabs_html=page_nav, main_title='Análise dos resultados', main_html=markup, bottom_html=bottom_markup)
        if recorder:
            print_static(output, ['europeias', key, 'analise'])
        return output

    else:
        return redirect(url_for('europeias_list'), code=302)

@app.route("/europeias/<key>/sim")
def europeias_sim(key):

    start_time = datetime.now()

    dict = europeias.init()

    if key in dict['files']:
        page_title = dict['files'][key]['title']

        # Carregar os dados
        status, json_path = assert_json(dict['basedir'], dict['files'][key]['csv'])
        if status == False:
            processed_data = europeias.process(dict['basedir'], dict['files'][key]['csv'])
            gen_json(json_path, processed_data)
            status = True
        retrieved_data = load_json(json_path)

        # Navegação vertical
        crumbs = europeias.crumbs(key, page_title, levels=['list','view','sim'])
        breadcrumb = Markup(render_template('components/breadcrumb.html', items=crumbs, active_page='europeias_sim'))
        breadcrumb += Markup(menu_html('Eleições', dict['files'], 'europeias_sim', component='dropdown'))

        # Navegação horizontal
        tabs = europeias.tabs(key)
        page_nav = Markup(render_template('components/nav-tabs.html', items=tabs, active_tab='europeias_sim'))

        # Conteúdo da página
        markup = ''

        df_v = retrieved_data['total']['df_v']
        #df_i = retrieved_data['total']['df_i']
        stats = retrieved_data['total']['stats']
        dict_meta = retrieved_data['meta']

        m_ind = [[retrieved_data['total']['alloc']['hondt']['diff_sum'], retrieved_data['total']['alloc']['hondt']['waste_sum'], '-'],
                [retrieved_data['total']['alloc']['sl']['diff_sum'], retrieved_data['total']['alloc']['sl']['waste_sum'], retrieved_data['total']['alloc']['sl']['swap']],
                [retrieved_data['total']['alloc']['dk']['diff_sum'], retrieved_data['total']['alloc']['dk']['waste_sum'], retrieved_data['total']['alloc']['dk']['swap']]]

        markup += Markup(methods_sim(df_v, dict_meta, m_ind))

        # Fundo da página
        bottom_markup = Markup(stats_html(stats))

        print('\nWEBAPP: página gerada em ' + str((datetime.now() - start_time).total_seconds()) + ' segundos.\n')
        output = render_template('no-sidebar.html', title=page_title, menu_parent='europeias_list', breadcrumb_html=breadcrumb, tabs_html=page_nav, main_title='Simulações', main_html=markup, bottom_html=bottom_markup)
        if recorder:
            print_static(output, ['europeias', key, 'sim'])
        return output

    else:
        return redirect(url_for('europeias_list'), code=302)

@app.route("/europeias/<key>/dados")
def europeias_data(key):

    start_time = datetime.now()

    dict = europeias.init()

    if key in dict['files']:
        page_title = dict['files'][key]['title']

        # Carregar os dados
        status, json_path = assert_json(dict['basedir'], dict['files'][key]['csv'])
        if status == False:
            processed_data = europeias.process(dict['basedir'], dict['files'][key]['csv'])
            gen_json(json_path, processed_data)
            status = True
        retrieved_data = load_json(json_path)

        # Navegação vertical
        crumbs = europeias.crumbs(key, page_title, levels=['list','view','data'])
        breadcrumb = Markup(render_template('components/breadcrumb.html', items=crumbs, active_page='europeias_data'))
        breadcrumb += Markup(menu_html('Eleições', dict['files'], 'europeias_data', component='dropdown'))

        # Navegação horizontal
        tabs = europeias.tabs(key)
        page_nav = Markup(render_template('components/nav-tabs.html', items=tabs, active_tab='europeias_data'))

        # Conteúdo da página
        markup = ''

        csv_title = retrieved_data['source']['title']
        markup += Markup('\n<p>' + csv_title + '</p>')

        df_v = retrieved_data['total']['df_v']
        df_i = retrieved_data['total']['df_i']
        stats = retrieved_data['total']['stats']
        dict_meta = retrieved_data['meta']

        comments = retrieved_data['source']['comments']
        markup += Markup(render_template('components/alert.html', variant='info', alert_text=comments))

        markup += Markup('\n<h3>Votos válidos</h3>')
        markup += Markup('\n<p>Percentagem dos votos calculada em função dos votos válidos expressos.</p>')

        df_info_abbr(df_v, 'Lista', dict_meta)
        df_v.rename(columns = {'v':'Votos',
                                    'v_pc':'%',
                                    'h':'Mandatos'}, inplace=True)
        data_cols = ['Lista','Votos','%','Mandatos']

        #markup += Markup('\n<div class="card my-3" style="overflow-x: auto">\n')
        markup += Markup(df_to_table(df_v, data_cols))
        #markup += Markup('\n</div>')

        markup += Markup('\n<h3>Votos inválidos</h3>')
        markup += Markup('\n<p>Percentagem dos votos calculada em função do total dos votos expressos.</p>')

        df_i.rename(columns = {'Lista':'',
                                    'i':'Votos',
                                    'i_pc':'%'}, inplace=True)
        data_cols = ['','Votos','%']

        #markup += Markup('\n<div class="card my-3" style="overflow-x: auto">\n')
        markup += Markup(df_to_table(df_i, data_cols))
        #markup += Markup('\n</div>')

        # Barra lateral
        sidebar_markup = Markup(stats_html(stats))

        print('\nWEBAPP: página gerada em ' + str((datetime.now() - start_time).total_seconds()) + ' segundos.\n')
        output = render_template('sidebar-right.html', title=page_title, menu_parent='europeias_list', breadcrumb_html=breadcrumb, tabs_html=page_nav, content_title='Dados oficiais', content_html=markup, sidebar_html=sidebar_markup)
        if recorder:
            print_static(output, ['europeias', key, 'dados'])
        return output

    else:
        return redirect(url_for('europeias_list'), code=302)

##
# Eleições legislativas
##

@app.route("/legislativas")
def legislativas_list():

    dict = legislativas.init()

    # Navegação vertical
    crumbs = legislativas.crumbs('', 'Legislativas', levels=['list'])
    breadcrumb = Markup(render_template('components/breadcrumb.html', items=crumbs, active_page='legislativas_list'))

    # Conteúdo da página
    markup = Markup(render_template('content/legislativas.html'))

    # Barra lateral
    sidebar_markup = Markup(menu_html('Resultados disponíveis', dict['files'], dict['route']))

    output = render_template('sidebar-right.html', title='Eleições legislativas', menu_parent='legislativas_list', breadcrumb_html=breadcrumb, content_html=markup, sidebar_html=sidebar_markup)
    if recorder:
        print_static(output, ['legislativas'])
    return output

# Primeiro nível de navegação

@app.route("/legislativas/<key>") # resultados
def legislativas_view(key):

    start_time = datetime.now()

    dict = legislativas.init()

    if key in dict['files']:
        page_title = dict['files'][key]['title']

        # Carregar os dados
        status, json_path = assert_json(dict['basedir'], dict['files'][key]['csv'])
        if status == False:
            processed_data = legislativas.process(dict['basedir'], dict['files'][key]['csv'])
            gen_json(json_path, processed_data)
            status = True
        retrieved_data = load_json(json_path)

        # Navegação vertical
        crumbs = legislativas.crumbs(key, page_title, levels=['list','view'])
        breadcrumb = Markup(render_template('components/breadcrumb.html', items=crumbs, active_page='legislativas_view'))
        breadcrumb += Markup(menu_html('Eleições', dict['files'], 'legislativas_view', component='dropdown'))

        # Navegação horizontal
        tabs = legislativas.tabs(key)
        page_nav = Markup(render_template('components/nav-tabs.html', items=tabs, active_tab='legislativas_view'))

        # Conteúdo da página
        csv_title = retrieved_data['source']['title']
        df_v = retrieved_data['total']['df_v']
        df_i = retrieved_data['total']['df_i']
        stats = retrieved_data['total']['stats']
        dict_meta = retrieved_data['meta']

        markup = Markup(results_table(csv_title, df_v, 'h_sum', df_i, stats, dict_meta))

        # Fundo da página
        bottom_markup = Markup(stats_html(stats))

        print('\nWEBAPP: página gerada em ' + str((datetime.now() - start_time).total_seconds()) + ' segundos.\n')
        output = render_template('no-sidebar.html', title=page_title, menu_parent='legislativas_list', breadcrumb_html=breadcrumb, tabs_html=page_nav, main_html=markup, bottom_html=bottom_markup)
        if recorder:
            print_static(output, ['legislativas', key])
        return output

    else:
        return redirect(url_for('legislativas_list'), code=302)

@app.route("/legislativas/<key>/dist") # círculos
def legislativas_dist(key):

    start_time = datetime.now()

    dict = legislativas.init()

    if key in dict['files']:
        page_title = dict['files'][key]['title']

        # Carregar os dados
        status, json_path = assert_json(dict['basedir'], dict['files'][key]['csv'])
        if status == False:
            processed_data = legislativas.process(dict['basedir'], dict['files'][key]['csv'])
            gen_json(json_path, processed_data)
            status = True
        retrieved_data = load_json(json_path)

        # Navegação vertical
        crumbs = legislativas.crumbs(key, page_title, levels=['list','view','dist'])
        breadcrumb = Markup(render_template('components/breadcrumb.html', items=crumbs, active_page='legislativas_dist'))
        breadcrumb += Markup(menu_html('Eleições', dict['files'], 'legislativas_dist', component='dropdown'))

        # Navegação horizontal
        tabs = legislativas.tabs(key)
        page_nav = Markup(render_template('components/nav-tabs.html', items=tabs, active_tab='legislativas_dist'))

        # Conteúdo da página
        markup = ''
        markup += Markup(render_template('content/quote-inutil.html'))

        d_split = retrieved_data['split']
        markup += Markup(dist_table(d_split))

        # Barra lateral
        d_list = list(d_split)
        dict_clean = clean_strings(d_list)
        dict_badges = {}
        d_iter = 0

        for d in dict_clean:
            new_key = d_list[d_iter] + ' <span class="badge badge-light float-right mt-1">' + str(d_split[d]['stats']['s']) + '</span>'
            dict_badges[new_key] = dict_clean[d]
            d_iter += 1

        d_menu_title = 'Círculos <span class="badge badge-light float-right mt-1">Mandatos</span>'
        sidebar_markup = Markup(d_menu_html(d_menu_title, key, dict_badges, 'legislativas_dist_view'))

        # Fundo da página
        bottom_markup = ''

        print('\nWEBAPP: página gerada em ' + str((datetime.now() - start_time).total_seconds()) + ' segundos.\n')
        output = render_template('sidebar-right.html', title=page_title, menu_parent='legislativas_list', breadcrumb_html=breadcrumb, tabs_html=page_nav, content_title='Círculos', content_html=markup, sidebar_html=sidebar_markup, bottom_html=bottom_markup)
        if recorder:
            print_static(output, ['legislativas', key, 'dist'])
        return output

    else:
        return redirect(url_for('legislativas_list'), code=302)

@app.route("/legislativas/<key>/analise")
def legislativas_analysis(key):

    start_time = datetime.now()

    dict = legislativas.init()

    if key in dict['files']:
        page_title = dict['files'][key]['title']

        # Carregar os dados
        status, json_path = assert_json(dict['basedir'], dict['files'][key]['csv'])
        if status == False:
            processed_data = legislativas.process(dict['basedir'], dict['files'][key]['csv'])
            gen_json(json_path, processed_data)
            status = True
        retrieved_data = load_json(json_path)

        # Navegação vertical
        crumbs = legislativas.crumbs(key, page_title, levels=['list','view','analysis'])
        breadcrumb = Markup(render_template('components/breadcrumb.html', items=crumbs, active_page='legislativas_analysis'))
        breadcrumb += Markup(menu_html('Eleições', dict['files'], 'legislativas_analysis', component='dropdown'))

        # Navegação horizontal
        tabs = legislativas.tabs(key)
        page_nav = Markup(render_template('components/nav-tabs.html', items=tabs, active_tab='legislativas_analysis'))

        # Conteúdo da página
        df_v = retrieved_data['total']['df_v']
        df_i = retrieved_data['total']['df_i']
        stats = retrieved_data['total']['stats']
        dict_meta = retrieved_data['meta']

        markup = Markup(analysis_sum(df_v, 'h_sum', stats, dict_meta))

        # Fundo da página
        stats = retrieved_data['total']['stats']
        bottom_markup = Markup(stats_html(stats))

        print('\nWEBAPP: página gerada em ' + str((datetime.now() - start_time).total_seconds()) + ' segundos.\n')
        output = render_template('no-sidebar.html', title=page_title, menu_parent='legislativas_list', breadcrumb_html=breadcrumb, tabs_html=page_nav, main_title='Análise dos resultados', main_html=markup, bottom_html=bottom_markup)
        if recorder:
            print_static(output, ['legislativas', key, 'analise'])
        return output

    else:
        return redirect(url_for('legislativas_list'), code=302)

@app.route("/legislativas/<key>/sim")
def legislativas_sim(key):

    start_time = datetime.now()

    dict = legislativas.init()

    if key in dict['files']:
        page_title = dict['files'][key]['title']

        # Carregar os dados
        status, json_path = assert_json(dict['basedir'], dict['files'][key]['csv'])
        if status == False:
            processed_data = legislativas.process(dict['basedir'], dict['files'][key]['csv'])
            gen_json(json_path, processed_data)
            status = True
        retrieved_data = load_json(json_path)

        # Navegação vertical
        crumbs = legislativas.crumbs(key, page_title, levels=['list','view','sim'])
        breadcrumb = Markup(render_template('components/breadcrumb.html', items=crumbs, active_page='legislativas_sim'))
        breadcrumb += Markup(menu_html('Eleições', dict['files'], 'legislativas_sim', component='dropdown'))

        # Navegação horizontal
        tabs = legislativas.tabs(key)
        page_nav = Markup(render_template('components/nav-tabs.html', items=tabs, active_tab='legislativas_sim'))

        # Conteúdo da página
        markup = ''

        df_v = retrieved_data['total']['df_v']
        #df_i = retrieved_data['total']['df_i']
        stats = retrieved_data['total']['stats']
        dict_meta = retrieved_data['meta']

        m_ind = [[retrieved_data['total']['agg']['hondt']['diff_sum'], retrieved_data['total']['agg']['hondt']['waste_sum'], '-'],
                [retrieved_data['total']['agg']['sl']['diff_sum'], retrieved_data['total']['agg']['sl']['waste_sum'], retrieved_data['total']['agg']['sl']['swap']],
                [retrieved_data['total']['agg']['dk']['diff_sum'], retrieved_data['total']['agg']['dk']['waste_sum'], retrieved_data['total']['agg']['dk']['swap']],
                [retrieved_data['total']['alloc']['hondt']['diff_sum'], retrieved_data['total']['alloc']['hondt']['waste_sum'], retrieved_data['total']['alloc']['hondt']['swap']],
                [retrieved_data['total']['alloc']['sl']['diff_sum'], retrieved_data['total']['alloc']['sl']['waste_sum'], retrieved_data['total']['alloc']['sl']['swap']],
                [retrieved_data['total']['alloc']['dk']['diff_sum'], retrieved_data['total']['alloc']['dk']['waste_sum'], retrieved_data['total']['alloc']['dk']['swap']]]

        markup += Markup(methods_sum(df_v, dict_meta, m_ind))

        # Fundo da página
        bottom_markup = Markup(stats_html(stats))

        print('\nWEBAPP: página gerada em ' + str((datetime.now() - start_time).total_seconds()) + ' segundos.\n')
        output = render_template('no-sidebar.html', title=page_title, menu_parent='legislativas_list', breadcrumb_html=breadcrumb, tabs_html=page_nav, main_title='Simulações', main_html=markup, bottom_html=bottom_markup)
        if recorder:
            print_static(output, ['legislativas', key, 'sim'])
        return output

    else:
        return redirect(url_for('legislativas_list'), code=302)

@app.route("/legislativas/<key>/dados")
def legislativas_data(key):

    start_time = datetime.now()

    dict = legislativas.init()

    if key in dict['files']:
        page_title = dict['files'][key]['title']

        # Carregar os dados
        status, json_path = assert_json(dict['basedir'], dict['files'][key]['csv'])
        if status == False:
            processed_data = legislativas.process(dict['basedir'], dict['files'][key]['csv'])
            gen_json(json_path, processed_data)
            status = True
        retrieved_data = load_json(json_path)

        # Navegação vertical
        crumbs = legislativas.crumbs(key, page_title, levels=['list','view','data'])
        breadcrumb = Markup(render_template('components/breadcrumb.html', items=crumbs, active_page='legislativas_data'))
        breadcrumb += Markup(menu_html('Eleições', dict['files'], 'legislativas_data', component='dropdown'))

        # Navegação horizontal
        tabs = legislativas.tabs(key)
        page_nav = Markup(render_template('components/nav-tabs.html', items=tabs, active_tab='legislativas_data'))

        # Conteúdo da página
        markup = Markup('<h2>Dados oficiais</h2><hr />')

        csv_title = retrieved_data['source']['title']
        markup += Markup('<p>' + csv_title + '</p>')

        comments = retrieved_data['source']['comments']
        markup += Markup(render_template('components/alert.html', variant='info', alert_text=comments))

        markup += Markup('<h3>Votos válidos</h3>')
        markup += Markup('<p>Percentagem dos votos calculada em função dos votos válidos expressos.</p>')

        df_v = retrieved_data['total']['df_v']

        df_info_abbr(df_v, 'Lista', retrieved_data['meta'])
        df_v.rename(columns = {'v':'Votos',
                                    'v_pc':'%',
                                    'h_sum':'Mandatos'}, inplace=True)
        data_cols = ['Lista','Votos','%','Mandatos']

        #markup += Markup('<div class="card my-3" style="overflow-x: auto">')
        markup += Markup(df_to_table(df_v, data_cols))
        #markup += Markup('</div>')

        markup += Markup('<h3>Votos inválidos</h3>')
        markup += Markup('<p>Percentagem dos votos calculada em função do total dos votos expressos.</p>')

        df_i = retrieved_data['total']['df_i']

        df_i.rename(columns = {'Lista':'',
                                    'i':'Votos',
                                    'i_pc':'%'}, inplace=True)
        data_cols = ['','Votos','%']

        #markup += Markup('<div class="card my-3" style="overflow-x: auto">')
        markup += Markup(df_to_table(df_i, data_cols))
        #markup += Markup('</div>')

        # Barra lateral
        stats = retrieved_data['total']['stats']
        sidebar_markup = Markup(stats_html(stats))

        print('\nWEBAPP: página gerada em ' + str((datetime.now() - start_time).total_seconds()) + ' segundos.\n')
        output = render_template('sidebar-right.html', title=page_title, menu_parent='legislativas_list', breadcrumb_html=breadcrumb, tabs_html=page_nav, content_html=markup, sidebar_html=sidebar_markup)
        if recorder:
            print_static(output, ['legislativas', key, 'dados'])
        return output

    else:
        return redirect(url_for('legislativas_list'), code=302)

# Segundo nível de navegação; círculos

@app.route("/legislativas/<key>/dist/<d_key>") # resultados
def legislativas_dist_view(key, d_key):

    start_time = datetime.now()

    dict = legislativas.init()

    if key in dict['files']:
        section_title = dict['files'][key]['title']

        # Carregar os dados
        status, json_path = assert_json(dict['basedir'], dict['files'][key]['csv'])
        if status == False:
            processed_data = legislativas.process(dict['basedir'], dict['files'][key]['csv'])
            gen_json(json_path, processed_data)
            status = True
        retrieved_data = load_json(json_path)

        # Divisão por círculos
        d_split = retrieved_data['split']
        d_list = list(d_split)
        dict_clean = clean_strings(d_list)
        if d_key in dict_clean.values():
            for d_name, d_clean in dict_clean.items():
                if d_key == d_clean:
                    d = d_name
            page_title = d

            # Navegação vertical
            crumbs = legislativas.d_crumbs(key, section_title, levels=['list','view','dist','dist_view'], d_key=d_key, d_title=d)
            breadcrumb = Markup(render_template('components/breadcrumb.html', items=crumbs, active_page='legislativas_dist_view'))
            breadcrumb += Markup(d_menu_html('Círculos', key, dict_clean, 'legislativas_dist_view', component='dropdown'))
            breadcrumb += Markup(k_menu_html('Eleições', d_key, dict['files'], 'legislativas_dist_view', component='dropdown'))

            # Navegação horizontal
            tabs = legislativas.d_tabs(key, d_key)
            page_nav = Markup(render_template('components/nav-tabs.html', items=tabs, active_tab='legislativas_dist_view'))

            # Conteúdo da página
            csv_title = retrieved_data['source']['title']
            df_v = retrieved_data['split'][d]['df_v']
            df_i = retrieved_data['split'][d]['df_i']
            stats = retrieved_data['split'][d]['stats']
            dict_meta = retrieved_data['meta']

            markup = Markup(results_table(csv_title, df_v, 'h', df_i, stats, dict_meta))

            # Fundo da página
            bottom_markup = Markup(stats_html(stats))

            print('\nWEBAPP: página gerada em ' + str((datetime.now() - start_time).total_seconds()) + ' segundos.\n')
            output = render_template('no-sidebar.html', section_title=section_title, title=page_title, menu_parent='legislativas_list', breadcrumb_html=breadcrumb, tabs_html=page_nav, main_html=markup, bottom_html=bottom_markup)
            if recorder:
                print_static(output, ['legislativas', key, 'dist', d_key])
            return output

        elif d_key == 'acores':
            page_title = 'Açores'

            # Navegação vertical
            crumbs = legislativas.d_crumbs(key, section_title, levels=['list','view','dist','dist_view'], d_key=d_key, d_title=page_title)
            breadcrumb = Markup(render_template('components/breadcrumb.html', items=crumbs, active_page='legislativas_dist_view'))
            breadcrumb += Markup(d_menu_html('Círculos', key, dict_clean, 'legislativas_dist_view', component='dropdown'))
            breadcrumb += Markup(k_menu_html('Eleições', d_key, dict['files'], 'legislativas_dist_view', component='dropdown'))

            # Conteúdo da página
            markup = Markup(render_template('content/acores.html'))

            # Barra lateral
            d_items = [
                        (url_for('legislativas_dist_view', key=key, d_key='angra-do-heroismo'),'Angra do Heroísmo'),
                        (url_for('legislativas_dist_view', key=key, d_key='horta'),'Horta'),
                        (url_for('legislativas_dist_view', key=key, d_key='ponta-delgada'),'Ponta Delgada')
                        ]
            sidebar_markup = Markup(render_template('components/list-group.html',items=d_items))

            output = render_template('sidebar-right.html', section_title=section_title, title=page_title, menu_parent='legislativas_list', breadcrumb_html=breadcrumb, content_html=markup, sidebar_html=sidebar_markup)
            if recorder:
                print_static(output, ['legislativas', '1976', 'dist', 'acores'])
            return output

        else:
            return redirect(url_for('legislativas_dist', key=key), code=302)

    else:
        return redirect(url_for('legislativas_list'), code=302)

@app.route("/legislativas/<key>/dist/<d_key>/analise")
def legislativas_dist_analysis(key, d_key):

    start_time = datetime.now()

    dict = legislativas.init()

    if key in dict['files']:
        section_title = dict['files'][key]['title']

        # Carregar os dados
        status, json_path = assert_json(dict['basedir'], dict['files'][key]['csv'])
        if status == False:
            processed_data = legislativas.process(dict['basedir'], dict['files'][key]['csv'])
            gen_json(json_path, processed_data)
            status = True
        retrieved_data = load_json(json_path)

        # Divisão por círculos
        d_split = retrieved_data['split']
        d_list = list(d_split)
        dict_clean = clean_strings(d_list)
        if d_key in dict_clean.values():
            for d_name, d_clean in dict_clean.items():
                if d_key == d_clean:
                    d = d_name
            page_title = d

            # Navegação vertical
            crumbs = legislativas.d_crumbs(key, section_title, levels=['list','view','dist','dist_view','dist_analysis'], d_key=d_key, d_title=d)
            breadcrumb = Markup(render_template('components/breadcrumb.html', items=crumbs, active_page='legislativas_dist_analysis'))
            breadcrumb += Markup(d_menu_html('Círculos', key, dict_clean, 'legislativas_dist_analysis', component='dropdown'))
            breadcrumb += Markup(k_menu_html('Eleições', d_key, dict['files'], 'legislativas_dist_analysis', component='dropdown'))

            # Navegação horizontal
            tabs = legislativas.d_tabs(key, d_key)
            page_nav = Markup(render_template('components/nav-tabs.html', items=tabs, active_tab='legislativas_dist_analysis'))

            # Conteúdo da página
            df_v = retrieved_data['split'][d]['df_v']
            df_i = retrieved_data['split'][d]['df_i']
            stats = retrieved_data['split'][d]['stats']
            seq_alloc = retrieved_data['split'][d]['alloc']['hondt']['seq']
            diff_sum = retrieved_data['split'][d]['alloc']['hondt']['diff_sum']
            waste_sum = retrieved_data['split'][d]['alloc']['hondt']['waste_sum']
            dict_meta = retrieved_data['meta']

            markup = Markup(analysis_single(df_v, 'h', stats, seq_alloc, dict_meta))

            # Barra lateral
            #sidebar_markup = Markup(stats_card(d, stats, diff_sum, waste_sum))

            print('\nWEBAPP: página gerada em ' + str((datetime.now() - start_time).total_seconds()) + ' segundos.\n')
            #return render_template('sidebar-right.html', section_title=section_title, title=page_title, menu_parent='legislativas_list', breadcrumb_html=breadcrumb, tabs_html=page_nav, content_title='Análise dos resultados', content_html=markup, sidebar_html=sidebar_markup)
            output = render_template('no-sidebar.html', section_title=section_title, title=page_title, menu_parent='legislativas_list', breadcrumb_html=breadcrumb, tabs_html=page_nav, main_title='Análise dos resultados', main_html=markup)
            if recorder:
                print_static(output, ['legislativas', key, 'dist', d_key, 'analise'])
            return output

        elif d_key == 'acores':
            page_title = 'Açores'

            # Navegação vertical
            crumbs = legislativas.d_crumbs(key, section_title, levels=['list','view','dist','dist_analysis'], d_key=d_key, d_title=page_title)
            breadcrumb = Markup(render_template('components/breadcrumb.html', items=crumbs, active_page='legislativas_dist_analysis'))
            breadcrumb += Markup(d_menu_html('Círculos', key, dict_clean, 'legislativas_dist_analysis', component='dropdown'))
            breadcrumb += Markup(k_menu_html('Eleições', d_key, dict['files'], 'legislativas_dist_analysis', component='dropdown'))

            # Conteúdo da página
            markup = Markup(render_template('content/acores.html'))

            # Barra lateral
            d_items = [
                        (url_for('legislativas_dist_analysis', key=key, d_key='angra-do-heroismo'),'Angra do Heroísmo'),
                        (url_for('legislativas_dist_analysis', key=key, d_key='horta'),'Horta'),
                        (url_for('legislativas_dist_analysis', key=key, d_key='ponta-delgada'),'Ponta Delgada')
                        ]
            sidebar_markup = Markup(render_template('components/list-group.html',items=d_items))

            output = render_template('sidebar-right.html', section_title=section_title, title=page_title, menu_parent='legislativas_list', breadcrumb_html=breadcrumb, content_html=markup, sidebar_html=sidebar_markup)
            if recorder:
                print_static(output, ['legislativas', '1976', 'dist', 'acores', 'analise'])
            return output

        else:
            return redirect(url_for('legislativas_dist', key=key), code=302)

    else:
        return redirect(url_for('legislativas_list'), code=302)

@app.route("/legislativas/<key>/dist/<d_key>/sim")
def legislativas_dist_sim(key, d_key):

    start_time = datetime.now()

    dict = legislativas.init()

    if key in dict['files']:
        section_title = dict['files'][key]['title']

        # Carregar os dados
        status, json_path = assert_json(dict['basedir'], dict['files'][key]['csv'])
        if status == False:
            processed_data = legislativas.process(dict['basedir'], dict['files'][key]['csv'])
            gen_json(json_path, processed_data)
            status = True
        retrieved_data = load_json(json_path)

        # Divisão por círculos
        d_split = retrieved_data['split']
        d_list = list(d_split)
        dict_clean = clean_strings(d_list)
        if d_key in dict_clean.values():
            for d_name, d_clean in dict_clean.items():
                if d_key == d_clean:
                    d = d_name
            page_title = d

            # Navegação vertical
            crumbs = legislativas.d_crumbs(key, section_title, levels=['list','view','dist','dist_view','dist_sim'], d_key=d_key, d_title=d)
            breadcrumb = Markup(render_template('components/breadcrumb.html', items=crumbs, active_page='legislativas_dist_sim'))
            breadcrumb += Markup(d_menu_html('Círculos', key, dict_clean, 'legislativas_dist_sim', component='dropdown'))
            breadcrumb += Markup(k_menu_html('Eleições', d_key, dict['files'], 'legislativas_dist_sim', component='dropdown'))

            # Navegação horizontal
            tabs = legislativas.d_tabs(key, d_key)
            page_nav = Markup(render_template('components/nav-tabs.html', items=tabs, active_tab='legislativas_dist_sim'))

            # Conteúdo da página
            markup = ''

            df_v = retrieved_data['split'][d]['df_v']
            stats = retrieved_data['split'][d]['stats']
            dict_meta = retrieved_data['meta']

            m_ind = [[retrieved_data['split'][d]['alloc']['hondt']['diff_sum'], retrieved_data['split'][d]['alloc']['hondt']['waste_sum'], '-'],
                    [retrieved_data['split'][d]['alloc']['sl']['diff_sum'], retrieved_data['split'][d]['alloc']['sl']['waste_sum'], retrieved_data['split'][d]['alloc']['sl']['swap']],
                    [retrieved_data['split'][d]['alloc']['dk']['diff_sum'], retrieved_data['split'][d]['alloc']['dk']['waste_sum'], retrieved_data['split'][d]['alloc']['dk']['swap']]]

            markup += Markup(methods_sim(df_v, dict_meta, m_ind))

            # Fundo da página
            bottom_markup = Markup(stats_html(stats))

            print('\nWEBAPP: página gerada em ' + str((datetime.now() - start_time).total_seconds()) + ' segundos.\n')
            output = render_template('no-sidebar.html', section_title=section_title, title=page_title, menu_parent='legislativas_list', breadcrumb_html=breadcrumb, tabs_html=page_nav, main_title='Simulações', main_html=markup, bottom_html=bottom_markup)
            if recorder:
                print_static(output, ['legislativas', key, 'dist', d_key, 'sim'])
            return output

        elif d_key == 'acores':
            page_title = 'Açores'

            # Navegação vertical
            crumbs = legislativas.d_crumbs(key, section_title, levels=['list','view','dist','dist_sim'], d_key=d_key, d_title=page_title)
            breadcrumb = Markup(render_template('components/breadcrumb.html', items=crumbs, active_page='legislativas_dist_sim'))
            breadcrumb += Markup(d_menu_html('Círculos', key, dict_clean, 'legislativas_dist_sim', component='dropdown'))
            breadcrumb += Markup(k_menu_html('Eleições', d_key, dict['files'], 'legislativas_dist_sim', component='dropdown'))

            # Conteúdo da página
            markup = Markup(render_template('content/acores.html'))

            # Barra lateral
            d_items = [
                        (url_for('legislativas_dist_sim', key=key, d_key='angra-do-heroismo'),'Angra do Heroísmo'),
                        (url_for('legislativas_dist_sim', key=key, d_key='horta'),'Horta'),
                        (url_for('legislativas_dist_sim', key=key, d_key='ponta-delgada'),'Ponta Delgada')
                        ]
            sidebar_markup = Markup(render_template('components/list-group.html',items=d_items))

            output = render_template('sidebar-right.html', section_title=section_title, title=page_title, menu_parent='legislativas_list', breadcrumb_html=breadcrumb, content_html=markup, sidebar_html=sidebar_markup)
            if recorder:
                print_static(output, ['legislativas', '1976', 'dist', 'acores', 'sim'])
            return output

        else:
            return redirect(url_for('legislativas_dist', key=key), code=302)

    else:
        return redirect(url_for('legislativas_list'), code=302)

@app.route("/legislativas/<key>/dist/<d_key>/dados")
def legislativas_dist_data(key, d_key):

    start_time = datetime.now()

    dict = legislativas.init()

    if key in dict['files']:
        section_title = dict['files'][key]['title']

        # Carregar os dados
        status, json_path = assert_json(dict['basedir'], dict['files'][key]['csv'])
        if status == False:
            processed_data = legislativas.process(dict['basedir'], dict['files'][key]['csv'])
            gen_json(json_path, processed_data)
            status = True
        retrieved_data = load_json(json_path)

        # Divisão por círculos
        d_split = retrieved_data['split']
        d_list = list(d_split)
        dict_clean = clean_strings(d_list)
        if d_key in dict_clean.values():
            for d_name, d_clean in dict_clean.items():
                if d_key == d_clean:
                    d = d_name
            page_title = d

            # Navegação vertical
            crumbs = legislativas.d_crumbs(key, section_title, levels=['list','view','dist','dist_view','dist_data'], d_key=d_key, d_title=d)
            breadcrumb = Markup(render_template('components/breadcrumb.html', items=crumbs, active_page='legislativas_dist_data'))
            breadcrumb += Markup(d_menu_html('Círculos', key, dict_clean, 'legislativas_dist_data', component='dropdown'))
            breadcrumb += Markup(k_menu_html('Eleições', d_key, dict['files'], 'legislativas_dist_data', component='dropdown'))

            # Navegação horizontal
            tabs = legislativas.d_tabs(key, d_key)
            page_nav = Markup(render_template('components/nav-tabs.html', items=tabs, active_tab='legislativas_dist_data'))

            # Conteúdo da página
            markup = Markup('<h2>Dados oficiais</h2><hr />')

            csv_title = retrieved_data['source']['title']
            markup += Markup('<p>' + csv_title + '</p>')

            comments = retrieved_data['source']['comments']
            markup += Markup(render_template('components/alert.html', variant='info', alert_text=comments))

            df_display = retrieved_data['split'][d]['df_v']
            df_info_abbr(df_display, 'Lista', retrieved_data['meta'])
            df_display.rename(columns = {'v':'Votos',
                                        'v_pc':'%',
                                        'h':'Mandatos'}, inplace=True)
            data_cols = ['Lista','Votos','%','Mandatos']

            markup += Markup('<h3>Votos válidos</h3>')
            markup += Markup('<p>Percentagem dos votos calculada em função dos votos válidos expressos.</p>')
            markup += Markup(df_to_table(df_display, data_cols))


            df_display = retrieved_data['total']['df_i']
            df_display.rename(columns = {'Lista':'',
                                        'i':'Votos',
                                        'i_pc':'%'}, inplace=True)
            data_cols = ['','Votos','%']

            markup += Markup('<h3>Votos inválidos</h3>')
            markup += Markup('<p>Percentagem dos votos calculada em função do total dos votos expressos.</p>')
            markup += Markup(df_to_table(df_display, data_cols))

            # Barra lateral
            stats = retrieved_data['split'][d]['stats']
            sidebar_markup = Markup(stats_html(stats))

            print('\nWEBAPP: página gerada em ' + str((datetime.now() - start_time).total_seconds()) + ' segundos.\n')
            output = render_template('sidebar-right.html', section_title=section_title, title=page_title, menu_parent='legislativas_list', breadcrumb_html=breadcrumb, tabs_html=page_nav, content_html=markup, sidebar_html=sidebar_markup)
            if recorder:
                print_static(output, ['legislativas', key, 'dist', d_key, 'dados'])
            return output

        elif d_key == 'acores':
            page_title = 'Açores'

            # Navegação vertical
            crumbs = legislativas.d_crumbs(key, section_title, levels=['list','view','dist','dist_data'], d_key=d_key, d_title=page_title)
            breadcrumb = Markup(render_template('components/breadcrumb.html', items=crumbs, active_page='legislativas_dist_data'))
            breadcrumb += Markup(d_menu_html('Círculos', key, dict_clean, 'legislativas_dist_data', component='dropdown'))
            breadcrumb += Markup(k_menu_html('Eleições', d_key, dict['files'], 'legislativas_dist_data', component='dropdown'))

            # Conteúdo da página
            markup = Markup(render_template('content/acores.html'))

            # Barra lateral
            d_items = [
                        (url_for('legislativas_dist_data', key=key, d_key='angra-do-heroismo'),'Angra do Heroísmo'),
                        (url_for('legislativas_dist_data', key=key, d_key='horta'),'Horta'),
                        (url_for('legislativas_dist_data', key=key, d_key='ponta-delgada'),'Ponta Delgada')
                        ]
            sidebar_markup = Markup(render_template('components/list-group.html',items=d_items))

            output = render_template('sidebar-right.html', section_title=section_title, title=page_title, menu_parent='legislativas_list', breadcrumb_html=breadcrumb, content_html=markup, sidebar_html=sidebar_markup)
            if recorder:
                print_static(output, ['legislativas', '1976', 'dist', 'acores', 'dados'])
            return output

        else:
            return redirect(url_for('legislativas_dist', key=key), code=302)

    else:
        return redirect(url_for('legislativas_list'), code=302)

# Conteúdo

@app.route("/sobre")
def about():
    markup = Markup(render_template('content/sobre.html'))
    links = [('https://dre.pt/web/guest/pesquisa-avancada/-/asearch/advanced/1/maximized?advanced.search=Pesquisa+Avan%C3%A7ada&texto=Mapa+oficial&order=whenSearchable&sortOrder=ASC&emissor=Comiss%C3%A3o+Nacional+de+Elei%C3%A7%C3%B5es&types=SERIEI', 'Diário da República'),
            ('http://www.cne.pt/', 'Comissão Nacional de Eleições'),
            ('https://www.eleicoes.mai.gov.pt/', 'Eleições - MAI'),
            ('https://pt.wikipedia.org/wiki/Elei%C3%A7%C3%B5es_legislativas_de_Portugal#Terceira_Rep%C3%BAblica_Portuguesa', 'Wikipédia: Eleições Legislativas')]
    sidebar_markup = Markup(render_template('components/list-group.html', external=True, header='Ligações úteis', items=links))
    output = render_template('sidebar-right.html', title='Sobre este projecto', menu_parent='about', content_html=markup, sidebar_html=sidebar_markup)
    if recorder:
        print_static(output, ['sobre'])
    return output

if __name__ == '__main__':
    app.run(debug=True)
