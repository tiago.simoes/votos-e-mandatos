import pandas as pd

# 1) Divisão por círculos eleitorais.

def df_d_split(df_src):

    # Obter as colunas do DataFrame
    data_cols = list(df_src)
    # Extrair as colunas com dados
    d_cols = data_cols[2:-1]
    # Inicializar o Dict de registo
    d_dict = {}

    for col in d_cols:
        # Partir das colunas de metadados
        d_df_cols = data_cols[0:2]
        # Adicionar a coluna de cada círculo
        d_df_cols.append(col)
        # Copiar as colunas para um novo DataFrame
        d_df = df_src[d_df_cols].copy()
        # Substituir o nome da coluna
        d_df.rename(columns = {col:'Total'}, inplace=True)
        # Eliminar linhas com zeros
        d_df = d_df[d_df['Total'] > 0]
        # Gravar na forma {d : df}
        d_dict[col] = d_df

    # Gravar o DataFrame dos totais
    d_total = data_cols[0:2]
    d_col = data_cols[-1]
    d_total.append(d_col)
    df_total = df_src[d_total].copy()
    df_total.rename(columns = {d_col:'Total'}, inplace=True)

    return d_dict, df_total

# 2) Divisão por tipo de resultado (válidos, inválidos, eleitores, mandatos).

def df_ctl_split(df_src):

    # Extrair a tabela de votos válidos
    df_v = df_src.copy()
    df_v = df_v[df_v.CTL == 'v']
    # Extrair a tabela de votos inválidos
    df_i = df_src.copy()
    df_i = df_i[df_i.CTL == 'i']
    # Extrair a tabela de eleitores inscritos
    df_u = df_src.copy()
    df_u = df_u[df_u.CTL == 'u']
    # Extrair a tabela de mandatos a atribuir
    df_s = df_src.copy()
    df_s = df_s[df_s.CTL == 's']

    return df_v, df_i, df_u, df_s

# 3) Estatísticas gerais, somatórios.

def df_stats(df_v, df_i, df_u, df_s):

    stats = {}

    v_sum = df_v['Total'].sum()
    stats['v_sum'] = int(v_sum)

    i_sum = df_i['Total'].sum()
    stats['i_sum'] = int(i_sum)

    vc_sum = v_sum + i_sum
    stats['vc_sum'] = int(vc_sum)

    u_sum = df_u['Total'].sum()
    stats['u'] = int(u_sum)

    absent = 1 - (vc_sum / u_sum)
    stats['absent'] = float(absent)

    s_sum = df_s['Total'].sum()
    stats['s'] = int(s_sum)

    return stats

# 4) Coluna de percentagem (divide uma coluna por um número).

def df_pc(df_src, col_src, divider, col_dest = 'Percentagem'):

    df_src[col_dest] = df_src[col_src] / divider

# 5) Coluna de proporção (divide uma coluna por outra).

def df_div(df_src, col_src, col_divide, col_dest = 'Proporção'):

    df_src[col_dest] = 0
    df_aux = df_src[df_src[col_divide] > 0].copy()

    for index, row in df_aux.iterrows():
        numerator = int(df_aux.loc[index,[col_src]])
        denominator = int(df_aux.loc[index,[col_divide]])
        if denominator > 0:
            df_src.loc[index,[col_dest]] = numerator / denominator

    del df_aux

# 6) Coluna de diferença (subtrai uma coluna a outra).

def df_diff(df_src, col_src, col_subtract, col_dest = 'Diferença'):

    df_src[col_dest] = df_src[col_src] - df_src[col_subtract]
    df_src['abs'] = abs(df_src[col_dest])
    diff_sum = df_src['abs'].sum()
    df_src.pop('abs')

    return diff_sum

# 7) Coluna de desperdício (soma resultados mediante parâmetro).

def df_w(df_src, col_src, col_param, col_dest = 'Desperdício'):

    v_waste = {}
    df_src[col_dest] = 0
    df_aux = df_src[df_src[col_param] == 0].copy()

    for index, row in df_aux.iterrows():
        empty = int(df_aux.loc[index,[col_src]])
        df_src.loc[index,[col_dest]] = empty
        v_waste[row['Lista']] = empty

    del df_aux

    df_src['abs'] = abs(df_src[col_dest])
    waste_sum = df_src['abs'].sum()
    df_src.pop('abs')

    return waste_sum, v_waste

# 8) Nova coluna a partir dum dicionário

def df_new(df_src, col_id, dict_src, col_dest = 'Resultados'):

    df_src[col_dest] = 0

    for key in dict_src:
        target = df_src[df_src[col_id] == key].index.values
        df_src.loc[target,[col_dest]] = dict_src[key]

# Interface de linha de comando
# Uso: python frames.py

if __name__ == '__main__':

    print('+-----------+')
    print('| frames.py |')
    print('+-----------+')

    print('\nContém funções de manipulação de DataFrames.\n')

    print('1) Divisão por círculos eleitorais.')
    print('2) Divisão por variável de controlo - CTL.')
    print('3) Estatísticas gerais, somatórios.')
    print('4) Coluna de percentagem (divide uma coluna por um escalar).')
    print('5) Coluna de proporção (divide uma coluna por outra).')
    print('6) Coluna de diferença (subtrai uma coluna a outra).')
    print('7) Coluna de desperdício (soma resultados mediante parâmetro).')
    print('8) Nova coluna a partir dum dicionário')

    print('\nNenhuma demonstração disponível.\n')
