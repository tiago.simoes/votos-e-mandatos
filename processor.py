from datetime import datetime
import unicodedata
import pandas as pd
from frames import df_ctl_split, df_stats, df_pc, df_div, df_diff, df_w, df_new
from allocation import alloc_ham, next_ham

# 1) Extrai informação das listas.

def df_info_extract(df):

    # Obter as colunas do DataFrame
    data_cols = list(df)

    # Extrair os nomes das colunas de metadados
    d_cols = data_cols[0:3]
    col_long = d_cols[0]    # 'Candidatura' ou similar
    col_list = d_cols[1]    # 'Lista'
    col_hex = d_cols[2]     # 'HEX'

    # Inicializar o Dict de registo
    dict_meta = {}
    for index, row in df.iterrows():
        dict_meta[row[col_list]] = (row[col_long], row[col_hex])

    # Eliminar as colunas de metadados
    df.pop(col_long)
    df.pop(col_hex)

    # Atribuir um nome genérico à última coluna
    total_col = data_cols[-1]
    df.rename(columns={total_col: 'Total'}, inplace=True)

    return dict_meta

# 2) Pré-processamento: calcula estatística geral e extrai resultados.

def df_preprocess(df):

    print('\nDataFrame extraído:\n')
    print(df)

    # Processamento de um só círculo
    df_v, df_i, df_u, df_s = df_ctl_split(df)
    stats = df_stats(df_v, df_i, df_u, df_s)

    df_i.rename(columns = {'Total':'i'}, inplace=True)
    df_pc(df_i,'i',stats['vc_sum'],'i_pc')

    df_v.rename(columns = {'Total':'v'}, inplace=True)
    df_pc(df_v,'v',stats['v_sum'],'v_pc')
    df_v = df_v.sort_values(by=['v'], ascending=False)

    return stats, df_i, df_v

# 3) Agregação: agrega e soma para colunas adicionais.

def df_aggregate(df_v, stats, d_alloc, methods, ref_agg = 'h_sum'):

    start_time = datetime.now()

    df_v = df_v.sort_values(by=['v'], ascending=False)

    s_agg = {}

    for method in methods:
        m_id, m_init, m_key = method
        print(m_key)
        m_key = m_key + '_sum'
        s_agg[m_id] = {}
        partial_s = {}

        # agregar as alocações de todos os círculos
        for d in d_alloc:
            d_agg = {}
            for line in d_alloc[d][m_id]['seq']:
                c, s, q = line
                d_agg[c] = s
            # Somar ao total
            for c in d_agg:
                if c in partial_s:
                    partial_s[c] += d_agg[c]
                else:
                    partial_s[c] = d_agg[c]

        df_new(df_v, 'Lista', partial_s, col_dest = m_key)

        # diferença entre alocações, quando esta não é a predefinida
        if ref_agg != m_key:
            diff_s = df_diff(df_v, m_key, ref_agg, (m_key + '-' + ref_agg))
            s_agg[m_id]['swap'] = int(diff_s / 2)

        # percentagem de mandatos
        df_pc(df_v, m_key, stats['s'], (m_key + '_pc'))

        # diferença entre percentagens
        s_agg[m_id]['diff_sum'] = df_diff(df_v, (m_key + '_pc'), 'v_pc', (m_key + '_dpc'))

        # votos por mandato obtido
        df_div(df_v, 'v', m_key, ('v/' + m_key))
        df_v[('v/' + m_key)] = df_v[('v/' + m_key)].astype(int)

        # agregar os desperdícios de todos os círculos
        partial_waste = {}
        waste_sum = 0
        for d in d_alloc:
            d_waste = {}
            for c, value in d_alloc[d][m_id]['v_waste'].items():
                d_waste[c] = value
            # Somar ao total
            for c in d_waste:
                waste_sum += d_waste[c]
                if c in partial_waste:
                    partial_waste[c] += d_waste[c]
                else:
                    partial_waste[c] = d_waste[c]

        df_new(df_v, 'Lista', partial_waste, col_dest = ('0' + m_key))
        # percentagem de desperdícios
        df_div(df_v, ('0' + m_key), 'v', ('0' + m_key + '_ratio'))

        # total de desperdiçados
        s_agg[m_id]['waste_sum'] = int(waste_sum)

        df_v = df_v.sort_values(by=['v'], ascending=False)

    return s_agg, df_v

# 4) Processamento: aloca mandatos num DataFrame segundo métodos indicados.

def df_process(df_v, stats, methods, ref_alloc = 'h'):

    start_time = datetime.now()

    df_v = df_v.sort_values(by=['v'], ascending=False)

    s_alloc = {}

    for method in methods:
        m_id, m_init, m_key = method
        s_alloc[m_id] = {}
        s_alloc[m_id]['seq'] = alloc_ham(df_v, 'Lista', 'v', stats['s'], col_s = m_key, init = m_init)
        # diferença entre alocações, quando esta não é a predefinida
        if ref_alloc != m_key:
            diff_s = df_diff(df_v, m_key, ref_alloc, (m_key + '-' + ref_alloc))
            s_alloc[m_id]['swap'] = int(diff_s / 2)
        # percentagem de mandatos
        df_pc(df_v, m_key, stats['s'], (m_key + '_pc'))
        # diferença entre percentagens
        s_alloc[m_id]['diff_sum'] = df_diff(df_v, (m_key + '_pc'), 'v_pc', (m_key + '_dpc'))
        # votos por mandato obtido
        df_div(df_v, 'v', m_key, ('v/' + m_key))
        df_v[('v/' + m_key)] = df_v[('v/' + m_key)].astype(int)
        # votos desperdiçados
        waste_sum, v_waste = df_w(df_v, 'v', m_key, ('0' + m_key))
        s_alloc[m_id]['v_waste'] = v_waste
        s_alloc[m_id]['waste_sum'] = int(waste_sum)
        # diferença para o último
        df_v = df_v.sort_values(by=[m_key], ascending=False)
        next_ham(df_v, 'Lista', 'v', m_key, s_alloc[m_id]['seq'], col_diff = (m_key + '_diff'), init=m_init)
        df_v[(m_key + '_diff')] = df_v[(m_key + '_diff')].astype(int)
        # proporção da diferença para o último
        df_div(df_v, (m_key + '_diff'), 'v', (m_key + '_ratio'))

        df_v = df_v.sort_values(by=['v'], ascending=False)

    print(df_v[['Lista','h','h_diff','h_ratio','sl','sl_diff','sl_ratio','dk','dk_diff','dk_ratio']])
    print('\nPROCESSOR: DataFrame processado em ' + str((datetime.now() - start_time).total_seconds()) + ' segundos.')

    return s_alloc, df_v

# 5) Pós-processamento: converte DataFrames em JSON antes de gravar.

def df_postprocess(df_i, df_v):

    # gravar o DataFrame como JSON
    json_i = df_i.to_json(orient='split')

    df_v = df_v.sort_values(by=['v'], ascending=False)
    # gravar o DataFrame como JSON
    json_v = df_v.to_json(orient='split')

    return json_i, json_v

# 6) Sanitiza texto para uso noutras funções.

def clean_strings(list):

    clean_dict = {}

    for item in list:
        clean = item
        clean = clean.lower()
        clean = clean.strip()
        clean = clean.replace(' ', '-')
        clean = unicodedata.normalize('NFKD', clean).encode('ascii', 'ignore')
        clean = clean.decode()
        clean_dict[item] = clean

    return clean_dict

# Interface de linha de comando
# Uso: python processor.py

if __name__ == '__main__':

    print('+--------------+')
    print('| processor.py |')
    print('+--------------+')

    print('\nContém funções de processamento de dados.\n')

    print('1) Extrai informação das listas.')
    print('2) Pré-processamento: calcula estatística geral e extrai resultados.')
    print('3) Agregação: agrega informação adicional, como colunas de somas.')
    print('4) Processamento: aloca mandatos num DataFrame segundo métodos indicados.')
    print('5) Pós-processamento: converte DataFrames em JSON antes de gravar.')
    print('6) Sanitiza texto para uso noutras funções.')

    print('\nNenhuma demonstração disponível.\n')
