ANOS=('1976' '1979' '1980' '1983' '1985' '1987' '1991' '1995' '1999' '2002' '2005' '2009' '2011' '2015' '2019' '2022' '2024')
DIST=('acores' 'aveiro' 'beja' 'braga' 'braganca' 'castelo-branco' 'coimbra' 'europa' 'evora' 'faro' 'fora-da-europa' 'guarda' 'leiria' 'lisboa' 'madeira' 'portalegre' 'porto' 'santarem' 'setubal' 'viana-do-castelo' 'vila-real' 'viseu')

BASE_URL='http://127.0.0.1:5000/legislativas'

for ANO in ${ANOS[@]}; do
  curl ${BASE_URL}'/'${ANO}
  curl ${BASE_URL}'/'${ANO}'/analise'
  curl ${BASE_URL}'/'${ANO}'/dados'
  curl ${BASE_URL}'/'${ANO}'/sim'

  for D in ${DIST[@]}; do
    curl ${BASE_URL}'/'${ANO}'/dist/'${D}
    curl ${BASE_URL}'/'${ANO}'/dist/'${D}'/analise'
    curl ${BASE_URL}'/'${ANO}'/dist/'${D}'/dados'
    curl ${BASE_URL}'/'${ANO}'/dist/'${D}'/sim'
  done
done
