from os import path
import pandas as pd
from utils import csv_to_df
from frames import df_d_split, df_ctl_split, df_stats, df_pc, df_diff
from allocation import alloc_ham, next_ham, alloc_agg
from processor import df_info_extract, df_preprocess, df_aggregate, df_process, df_postprocess

# 1) Elenca os ficheiros de resultados disponíveis e informação associada.

def init():

    dict = {
        'route' : 'legislativas_view',
        'basedir' : 'legislativas',
        'files' : {
            '2024' : {
                'csv' : 'ar_2024.csv',
                'title' : 'Eleições legislativas, 2024'
            },
            '2022' : {
                'csv' : 'ar_2022.csv',
                'title' : 'Eleições legislativas, 2022'
            },
            '2019' : {
                'csv' : 'ar_2019.csv',
                'title' : 'Eleições legislativas, 2019'
            },
            '2015' : {
                'csv' : 'ar_2015.csv',
                'title' : 'Eleições legislativas, 2015'
            },
            '2011' : {
                'csv' : 'ar_2011.csv',
                'title' : 'Eleições legislativas, 2011'
            },
            '2009' : {
                'csv' : 'ar_2009.csv',
                'title' : 'Eleições legislativas, 2009'
            },
            '2005' : {
                'csv' : 'ar_2005.csv',
                'title' : 'Eleições legislativas, 2005'
            },
            '2002' : {
                'csv' : 'ar_2002.csv',
                'title' : 'Eleições legislativas, 2002'
            },
            '1999' : {
                'csv' : 'ar_1999.csv',
                'title' : 'Eleições legislativas, 1999'
            },
            '1995' : {
                'csv' : 'ar_1995.csv',
                'title' : 'Eleições legislativas, 1995'
            },
            '1991' : {
                'csv' : 'ar_1991.csv',
                'title' : 'Eleições legislativas, 1991'
            },
            '1987' : {
                'csv' : 'ar_1987.csv',
                'title' : 'Eleições legislativas, 1987'
            },
            '1985' : {
                'csv' : 'ar_1985.csv',
                'title' : 'Eleições legislativas, 1985'
            },
            '1983' : {
                'csv' : 'ar_1983.csv',
                'title' : 'Eleições legislativas, 1983'
            },
            '1980' : {
                'csv' : 'ar_1980.csv',
                'title' : 'Eleições legislativas, 1980'
            },
            '1979' : {
                'csv' : 'ar_1979.csv',
                'title' : 'Eleições legislativas, 1979'
            },
            '1976' : {
                'csv' : 'ar_1976.csv',
                'title' : 'Eleições legislativas, 1976'
            }
        }
    }

    return dict

# 2) Fornece a estrutura de navegação vertical (breadcrumb).

def crumbs(key, page_title, levels=['list']):

    crumbs = [
        ('/', 'index', 'Início'),
    ]

    structure = {
        'list'      : ('/legislativas', 'legislativas_list', 'Legislativas'),
        'view'      : ('/legislativas/' + key, 'legislativas_view', page_title),
        'analysis'  : ('/legislativas/' + key + '/analise', 'legislativas_analysis', 'Análise'),
        'sim'       : ('/legislativas/' + key + '/sim', 'legislativas_sim', 'Simulações'),
        'data'      : ('/legislativas/' + key + '/dados', 'legislativas_data', 'Dados'),
        'dist'      : ('/legislativas/' + key + '/dist', 'legislativas_dist', 'Círculos')
    }

    for level in levels:
        crumbs.append(structure[level])

    return crumbs

def d_crumbs(key, page_title, levels=['list'], d_key = 'null', d_title = ''):

    crumbs = [
        ('/', 'index', 'Início'),
    ]

    structure = {
        'list'          : ('/legislativas', 'legislativas_list', 'Legislativas'),
        'view'          : ('/legislativas/' + key, 'legislativas_view', page_title),
        'dist'          : ('/legislativas/' + key + '/dist', 'legislativas_dist', 'Círculos'),
        'dist_view'     : ('/legislativas/' + key + '/dist/' + d_key, 'legislativas_dist_view', d_title),
        'dist_analysis' : ('/legislativas/' + key + '/dist/' + d_key + '/analise', 'legislativas_dist_analysis', 'Análise'),
        'dist_sim'      : ('/legislativas/' + key + '/dist/' + d_key + '/sim', 'legislativas_dist_sim', 'Simulações'),
        'dist_data'     : ('/legislativas/' + key + '/dist/' + d_key + '/dados', 'legislativas_dist_data', 'Dados'),
    }

    for level in levels:
        crumbs.append(structure[level])

    return crumbs

# 3) Fornece a estrutura de navegação horizontal (tabs).

def tabs(key):

    tabs = [
        ('/legislativas/' + key, 'legislativas_view', 'Resultados'),
        ('/legislativas/' + key + '/analise', 'legislativas_analysis', 'Análise'),
        ('/legislativas/' + key + '/sim', 'legislativas_sim', 'Simulações'),
        ('/legislativas/' + key + '/dados', 'legislativas_data', 'Dados'),
        ('/legislativas/' + key + '/dist', 'legislativas_dist', 'CÍRCULOS')
    ]

    return tabs

def d_tabs(key, d_key):

    tabs = [
        ('/legislativas/' + key + '/dist', 'legislativas_dist', '« CÍRCULOS'),
        ('/legislativas/' + key + '/dist/' + d_key, 'legislativas_dist_view', 'Resultados'),
        ('/legislativas/' + key + '/dist/' + d_key + '/analise', 'legislativas_dist_analysis', 'Análise'),
        ('/legislativas/' + key + '/dist/' + d_key + '/sim', 'legislativas_dist_sim', 'Simulações'),
        ('/legislativas/' + key + '/dist/' + d_key + '/dados', 'legislativas_dist_data', 'Dados'),
    ]

    return tabs

# 4) Cria a estrutura de dados para processamento e devolve os resultados.

def process(dir, file):

    file_path = path.join(dir, file)
    print('\nLEGISLATIVAS: a processar o ficheiro ' + file_path)

    # esquema geral da informação
    output = {
        'source': {},
        'meta': {},
        'stats': {},
        'total': {},
        'split': {}
    }

    # detalhes da origem dos dados
    output['source'] = {
        'dir': dir,
        'file': file,
        'mtime': path.getmtime(file_path)
    }

    # informação no ficheiro
    df, title, comments = csv_to_df(file_path)

    output['source']['title'] = title
    output['source']['comments'] = comments

    # preformatação
    output['meta'] = df_info_extract(df)

    col_total = list(df)[-1]
    df.rename(columns = {col_total:'Total'}, inplace=True)

    # desdobramento
    d_split, df_total = df_d_split(df)
    d_alloc = {}

    # métodos a utilizar
    # (m_id, m_init, m_key)
    methods = [
        ('hondt', 1, 'h'),
        ('sl', 2, 'sl'),
        ('dk', 3, 'dk')
    ]

    for d in d_split:
        output['split'][d] = {}

        # pré-processamento dos dados
        stats, df_i, df_v = df_preprocess(d_split[d])

        output['split'][d]['stats'] = stats

        # processamento dos dados
        s_alloc, df_v = df_process(df_v, stats, methods)

        output['split'][d]['alloc'] = s_alloc

        # pós-processamento dos dados
        json_i, json_v = df_postprocess(df_i, df_v)

        output['split'][d]['df_i'] = json_i
        output['split'][d]['df_v'] = json_v

        d_alloc[d] = s_alloc

    # protocolo específico do total nacional

    # pré-processamento dos dados
    stats, df_i, df_v = df_preprocess(df_total)

    output['total']['stats'] = stats

    # agregação de dados do desdobramento
    s_agg, df_v = df_aggregate(df_v, stats, d_alloc, methods)
    output['total']['agg'] = s_agg

    # processamento dos dados
    s_alloc, df_v = df_process(df_v, stats, methods, ref_alloc = 'h_sum')

    output['total']['alloc'] = s_alloc

    # pós-processamento dos dados
    json_i, json_v = df_postprocess(df_i, df_v)

    output['total']['df_i'] = json_i
    output['total']['df_v'] = json_v

    return output

# Interface de linha de comando
# Uso: python legislativas.py

if __name__ == '__main__':

    print('+-----------------+')
    print('| legislativas.py |')
    print('+-----------------+')

    print('\nContém funções específicas do processamento de resultados das legislativas.\n')

    print('1) Elenca os ficheiros de resultados disponíveis e informação associada.')
    print('2) Fornece a estrutura de navegação vertical (breadcrumb).')
    print('3) Fornece a estrutura de navegação horizontal (tabs).')
    print('4) Cria a estrutura de dados para processamento e devolve os resultados.')

    print('\nNenhuma demonstração disponível.\n')
