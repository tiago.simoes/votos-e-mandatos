# Votos e Mandatos

Aplicação para alocar mandatos em função de distribuição de votos.

## 1. Introdução

**Votos e Mandatos** é uma aplicação que visa facilitar a visualização e análise de alocação de mandatos em contexto de eleições.

Esta aplicação permite:

  - leitura de distribuição de votos a partir de ficheiros devidamente formatados;
  - análise estatística de distribuição de votos;
  - conversão de votos em mandatos segundo diferentes métodos;
  - comparação entre distribuição de votos e de mandatos;
  - visualização de dados e comparações;
  - geração de código HTML estático para publicação.

## 2. Utilização

Esta aplicação é escrita em Python e inclui uma série de ferramentas para facilitar a sua portabilidade.

### 2.1. Requisitos

Python v3.6 ou superior, com o módulo *venv* instalado.

### 2.2. Instalação

Para instalar a aplicação, é necessário:

  - copiar os ficheiros do repositório do projecto

        git clone ...

  - iniciar o ambiente virtual (que usará a mesma versão de Python instalada no sistema, utilizada na sua criação)

        $ cd votos/
        $ python3 -m venv venv
        $ source venv/bin/activate

  *Para saber mais sobre a utilização de ambientes virtuais em Python, há um bom tutorial (em inglês) no [YouTube](https://www.youtube.com/watch?v=Kg1Yvry_Ydk)*

  - instalar os pacotes necessários e suas dependências, listados no ficheiro *requirements.txt"*

        (venv) $ pip install -r requirements.txt

  - finalmente, para sair do ambiente virtual:

        (venv) $ deactivate

### 2.3. Linha de comando

Os diferentes módulos incluídos na aplicação podem ser executados a partir da linha de comando individualmente, para obter uma demonstração simples da sua funcionalidade.

### 2.4. Aplicação *web* (local)

A aplicação *web*, construída com [Flask], corre localmente a partir dum servidor minimamente funcional criado para esse efeito.

Para lançar a aplicação *web*:

    $ env FLASK_APP=webapp.py flask run

Para correr a aplicação *web* em modo *debug*:

    $ python webapp.py

## 3. Dados

A aplicação está preparada para ler ficheiros CSV formatados como o ficheiro *sample.csv* - localizado no directório *data/*.

A primeira linha do ficheiro deve conter o título do conjunto de dados sob a forma de comentário (ver abaixo), visto que a aplicação assim o assume. Outras linhas comentadas no ficheiro também serão registadas pela aplicação, mas poderão não ser impressas no contexto desejado.

Para obter um ficheiro CSV devidamente formatado basta correr o módulo *utils* e gerar um exemplo que será gravado no directório *tmp/*.

### 3.1. Comentários

A aplicação considera o símbolo **%** como indicador de comentário, ou seja, todo o conteúdo de uma linha num ficheiro que seja antecedido por este símbolo será ignorado.

**Importante:** nenhum ficheiro CSV deve conter percentagens. Valores normalmente descritos em percentagem deve ser introduzidos como decimais e posteriormente formatados para serem impressos em formato de percentagem.

## 4. Créditos

Autoria:

  - TS
  - RAC

Contributos:

  - são bem-vindos!
