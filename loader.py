from datetime import datetime
from os import path
import json
import pandas as pd
from utils import list_files

# definições gerais
json_dir = 'tmp'

if not path.exists(json_dir):
    makedirs(json_dir)

def assert_json(data_dir, file_name):
    json_files = list_files(json_dir)
    file_path = path.join(data_dir, file_name)
    src_mtime = path.getmtime(file_path)

    base_name, ext = path.splitext(path.basename(file_name))
    json_path = path.join(json_dir, base_name + '.json')

    print('\nLOADER: ' + file_path + ' >> ' + json_path)

    # verificar se existe uma estrutura de dados já processada
    if json_path in json_files:
        print('\nLOADER: JSON está disponível.')
        # verificar se essa estrutura de dados está actualizada
        json_mtime = path.getmtime(json_path)
        if json_mtime > src_mtime:
            print('\nLOADER: JSON está actualizado.')
            return True, json_path
        else:
            print('\nLOADER: JSON está desactualizado.')
            return False, json_path

    else:
        print('\nLOADER: JSON inexistente.')
        return False, json_path

# gravar a estrutura de dados no ficheiro
def gen_json(json_path, data):
    start_time = datetime.now()
    with open(json_path, 'w+') as f:
        json.dump(data, f, indent=2)
    print('\nLOADER: JSON gerado em ' + str((datetime.now() - start_time).total_seconds()) + ' segundos.')

# extrair a estrutura de dados a partir do ficheiro
def load_json(json_path):
    start_time = datetime.now()
    with open(json_path, 'r') as f:
        data = json.load(f)

    # descodificar os DataFrames incluídos
    data['total']['df_i'] = pd.read_json(data['total']['df_i'], orient='split')
    data['total']['df_v'] = pd.read_json(data['total']['df_v'], orient='split')

    if 'split' in data:
        for d in data['split']:
            data['split'][d]['df_i'] = pd.read_json(data['split'][d]['df_i'], orient='split')
            data['split'][d]['df_v'] = pd.read_json(data['split'][d]['df_v'], orient='split')

    print('\nLOADER: JSON carregado em ' + str((datetime.now() - start_time).total_seconds()) + ' segundos.')

    return data

# Interface de linha de comando
# Uso: python loader.py

if __name__ == '__main__':

    print('+-----------+')
    print('| loader.py |')
    print('+-----------+')

    print('\nContém funções para verificar, gerar e ler ficheiros JSON.\n')

    print('1) Verifica a existência dum ficheiro JSON actualizado.')
    print('2) Gera um ficheiro JSON a partir duma estrutura de dados.')
    print('3) Carrega e descodifica um ficheiro JSON para uma estrutura de dados.')

    print('\nNenhuma demonstração disponível.\n')
