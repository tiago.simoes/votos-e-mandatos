from os import scandir, path, makedirs, unlink
import random, math, datetime
import numpy as np
import pandas as pd
from pick import pick

# Manutenção

if not path.exists('tmp'):
    makedirs('tmp')

# Funções de leitura e escrita

def list_files(directory):

    files = []

    with scandir(directory) as it:
        for entry in it:
            if not entry.name.startswith('.') and entry.is_file():
                filepath = path.join(directory,entry.name)
                files.append(filepath)

    return files

def csv_to_df(filename):

    # Importar o conteúdo tabular
    df = pd.read_csv(filename, comment='%')
    # Converter NaN em zero
    df = df.fillna(0)
    # Impôr formato inteiro nas colunas numéricas
    df_cols = list(df)
    num_cols = df_cols[4:]
    for col in num_cols:
        df[col] = df[col].astype(int)

    # Extrair as linhas comentadas
    comments = []
    file = open(filename,'r')
    for line in file:
        if line[0] == '%':
            # Salta o primeiro caractere e elimina espaço em branco
            comments.append(line[1:].strip())

    # Retirar o título da primeira linha comentada
    title = comments[0]
    comments.pop(0)

    return df, title, comments

def df_to_csv(df, filename = 'example.csv', title = 'CSV gerado pela aplicação', comments = ''):

    path = 'tmp/' + filename
    file = open(path,'w+')
    timestamp = datetime.datetime.now()

    # Escrever o título no início do ficheiro
    file.write('% ' + title + '\n')
    # Escrever o DataFrame
    df.to_csv(path_or_buf=file,index=False)
    # Escrever os comentários
    if comments != '':
        file.write('% ' + comments + '\n')
    # Adicionar metadados
    file.write('% Ficheiro gerado em ' + str(timestamp) + '\n')

    return path

def file_prompt():

    filename = input('Nome do ficheiro: ')
    if filename == '':
        filename = 'example.csv'

    title = input('Título: ')
    if title == '':
        title = 'CSV gerado pela aplicação'

    comments = input('Comentários: ')

    return filename, title, comments

# Funções de geração

def gen_example_df():

    # Considerando 2 círculos
    c1 = []
    c2 = []

    random.seed()

    # Votos válidos em 4 partidos
    for i in range(0,4):
        c1.append(random.randrange(20000,120000))
        c2.append(random.randrange(50000,150000))

    # Votos válidos num só círculos

    c1.append(0)
    c2.append(random.randrange(5000,15000))

    # Votos brancos e nulos
    for i in range(0,2):
        c1.append(random.randrange(2000,12000))
        c2.append(random.randrange(5000,15000))

    # Eleitores inscritos e mandatos correspondentes
    v1 = 0
    for i in c1:
        v1 += i
    u1 = int(v1 * (random.randrange(20,50,10) + 100) / 100)
    c1.append(u1)
    c1.append(int(math.ceil(u1 / 50000)))

    v2 = 0
    for i in c2:
        v2 += i
    u2 = int(v2 * (random.randrange(10,40,10) + 100) / 100)
    c2.append(u2)
    c2.append(int(math.ceil(u2 / 50000)))

    # Calcular totais
    ct = []
    for i in range(0,9):
        ct.append(c1[i] + c2[i])

    # Gerar o DataFrame de exemplo
    df = pd.DataFrame(
    	[['Partido A','PA','#49c219','v',c1[0],c2[0],ct[0]],
        ['Partido B','PB','#c21949','v',c1[1],c2[1],ct[1]],
        ['Partido C','PC','#1979c2','v',c1[2],c2[2],ct[2]],
        ['Partido D','PD','#c25419','v',c1[3],c2[3],ct[3]],
        ['Partido E','PE','#c20f19','v',c1[4],c2[4],ct[4]],
        ['Votos em branco','Brancos','#cccccc','i',c1[5],c2[5],ct[5]],
        ['Votos anulados','Nulos','#333333','i',c1[6],c2[6],ct[6]],
        ['Eleitores inscritos','Inscritos','#888888','u',c1[7],c2[7],ct[7]],
        ['Lugares a eleger','Lugares','#8888dd','s',c1[8],c2[8],ct[8]]],
    	index = [0,1,2,3,4,5,6,7,8],
    	columns = ['Nome','Lista','HEX','CTL','Círculo 1','Círculo 2','TOTAL'])

    return df

def gen_zeros_df(d, c):

    # Gerar o cabeçalho da tabela
    d_index = list(range(0,int(d)))
    header = ['Nome','Lista','HEX','CTL']

    # Gerar círculos eleitorais
    for index in d_index:
        header.append('Círculo ' + str(index + 1))
    header.append('TOTAL')

    # Gerar candidaturas e respectivos resultados
    c_index = list(range(0,int(c)))
    r_count = int(c) + 4
    r_index = list(range(0,r_count))
    rows = []
    zeros = np.zeros((int(d) + 1), dtype=int).tolist()
    for index in c_index:
        row = []
        row.append('Partido ' + str(index + 1))
        row.append('P' + str(index + 1))
        row.append('#cccccc')
        row.append('v')
        row.extend(zeros)
        rows.append(row)

    # Gerar o rodapé da tabela com dados gerais
    c_blank = ['Votos em branco', 'Brancos', '#cccccc', 'i']
    c_blank.extend(zeros)
    rows.append(c_blank)
    c_null = ['Votos nulos', 'Nulos', '#cccccc', 'i']
    c_null.extend(zeros)
    rows.append(c_null)
    c_registered = ['Eleitores inscritos', 'Inscritos', '#cccccc', 'u']
    c_registered.extend(zeros)
    rows.append(c_registered)
    c_seats = ['Número de mandatos', 'Mandatos', '#cccccc', 's']
    c_seats.extend(zeros)
    rows.append(c_seats)

    # Gerar o DataFrame resultante
    df = pd.DataFrame(data=rows,index=r_index,columns=header)

    return df

# Funções de demonstração

def tour():

    print('+----------+')
    print('| utils.py |')
    print('+----------+')

    print('\nEste módulo contém funções de leitura, geração e escrita de dados.')
    print('\nEstas funções podem ser utilizadas a partir da linha de comandos.')

    input('\nPrima ENTER para continuar...')

    print('\n--> list_files(directory)')
    print('-------------------------')
    print('Lista os ficheiros dentro de um directório (não recursivo).\n')

    list_dir = 'data'
    print('Directório predefinido: ' + list_dir)

    data_files = list_files(list_dir)
    print('\nFicheiros encontrados:')
    for file in data_files:
        print(file)

    input('\nPrima ENTER para continuar...')

    print('\n--> csv_to_df(filename)')
    print('-----------------------')
    print('Importa um ficheiro CSV para um DataFrame.\n')
    input('Prima ENTER para seleccionar...')

    title = 'Seleccione um ficheiro'
    opt_file, opt_file_index = pick(data_files, title)

    print('\nFicheiro seleccionado: ' + opt_file)

    sample_filename = opt_file
    sample_df, sample_title, sample_comments = csv_to_df(sample_filename)

    print('\nTítulo:')
    print(sample_title)

    print('\nDataFrame:')
    print(sample_df)

    print('\nComentários:')
    for line in sample_comments:
        print(line)

    input('\nPrima ENTER para continuar...')

    print('\n--> generate_df()')
    print('-----------------')
    print('Gera um DataFrame (meio) aleatório.')

    example_df = gen_example_df()

    print('\nDataFrame gerado:')
    print(example_df)

    input('\nPrima ENTER para continuar...')

    print('\n--> df_to_csv(df, filename, title, comments)')
    print('--------------------------------------------')
    print('Exporta um DataFrame para um ficheiro CSV.')
    print('Este ficheiro CSV tem o formato adequado para ser correctamente interpretado por esta aplicação.')

    example_filename = 'example.csv'
    example_title = 'Exemplo'
    example_comments = 'Ficheiro criado pelo gerador em utils.py'

    example_path = df_to_csv(example_df, example_filename, example_title, example_comments)

    print('\nFicheiro gravado em:')
    print(example_path)

    print('\nFim da demonstração.')

def launch():

    menu_title = 'VOTOS: utils.py\nSeleccione uma opção:'
    menu_options = ['Sair',
                    'Demonstração',
                    'Listar ficheiros em data/',
                    'Importar CSV e imprimir',
                    'Gerar CSV (meio) aleatório',
                    'Gerar CSV para preencher',
                    'Eliminar ficheiros em tmp/']

    menu_item, menu_index = pick(menu_options, menu_title)

    if menu_index == 1:
        tour()

    elif menu_index == 2:
        print('\n--> ' + menu_item + '\n')
        data_files = list_files('data')
        for file in data_files:
            print(file)

    elif menu_index == 3:
        print('\n--> ' + menu_item + '\n')
        import_filename = input('Nome do ficheiro: ')
        import_df, import_title, import_comments = csv_to_df(import_filename)
        print('\nTítulo:' + import_title)
        print('\nDataFrame:')
        print(import_df)
        print('\nComentários:')
        for line in import_comments:
            print(line)

    elif menu_index == 4:
        print('\n--> ' + menu_item + '\n')
        gen_rand_df = gen_example_df()
        print('DataFrame gerado:\n')
        print(gen_rand_df)
        input('\nPrima ENTER para continuar...')
        opt, opt_index = pick(['Sim', 'Não'],'Gravar ficheiro?')
        if opt_index == 0:
            filename, title, comments = file_prompt()
            gen_rand_path = df_to_csv(gen_rand_df, filename, title, comments)
            print('\nFicheiro gerado: ' + gen_rand_path)
        else:
            print('\nNenhum ficheiro gerado.')

    elif menu_index == 5:
        print('\n--> ' + menu_item + '\n')
        num_empty_dist = input('Número de círculos: ')
        num_empty_cand = input('Número de Nomes: ')
        gen_empty_df = gen_zeros_df(num_empty_dist,num_empty_cand)
        print('DataFrame gerado:')
        print(gen_empty_df)
        input('\nPrima ENTER para continuar...')
        opt, opt_index = pick(['Sim', 'Não'],'Gravar ficheiro?')
        if opt_index == 0:
            filename, title, comments = file_prompt()
            gen_empty_path = df_to_csv(gen_empty_df, filename, title, comments)
            print('\nFicheiro gerado: ' + gen_empty_path)
        else:
            print('\nNenhum ficheiro gerado.')

    elif menu_index == 6:
        print('\n--> ' + menu_item + '\n')
        tmp_files = list_files('tmp')
        if len(tmp_files) == 0:
            print('Não existem ficheiros em tmp/')
        else:
            for file in tmp_files:
                print(file)
            input('\nPrima ENTER para continuar...')
            opt, opt_index = pick(['Sim', 'Não'],'Apagar ficheiros?')
            if opt_index == 0:
                tmp_files = list_files('tmp')
                for file in tmp_files:
                    unlink(file)
                print('\nApagados todos os ficheiros em tmp/')

    return menu_index

# Interface de linha de comando
# Uso: python utils.py

if __name__ == '__main__':
    menu_index = -1
    while menu_index != 0:
        menu_index = launch()
        if menu_index == 0:
            print('\nObrigado e volte sempre!')
        else:
            input('\nPrima ENTER para regressar ao menu.')
