<!DOCTYPE html>
<html lang="en">
 <head>
  <!-- Required meta tags -->
  <meta charset="utf-8"/>
  <meta content="width=device-width, initial-scale=1, shrink-to-fit=no" name="viewport"/>
  <!-- Bootstrap CSS -->
  <link crossorigin="anonymous" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" rel="stylesheet"/>
  <!-- Custom styles -->
  <style media="screen">
   html,
    body {
      height: 100%;
    }
    body {
      display: flex;
      flex-direction: column;
    }
    body > header {
      position: absolute;
      width: 100%;
      height: 3.5rem;
    }
    body > main {
      flex: 1 0 auto;
      padding-top: 3.5rem;
    }
    body > footer {
      flex-shrink: 0;
    }
    .container-fluid {
      max-width: 80rem;
    }
  </style>
  <title>
   Eleições legislativas, 1980 | Votos e Mandatos
  </title>
 </head>
 <body class="bg-light">
  <header>
   <nav class="navbar fixed-top navbar-expand-lg navbar-dark bg-dark">
    <div class="container-fluid">
     <div class="navbar-brand">
      Votos e Mandatos
     </div>
     <button aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation" class="navbar-toggler" data-target="#navbarNav" data-toggle="collapse" type="button">
      <span class="navbar-toggler-icon">
      </span>
     </button>
     <div class="collapse navbar-collapse" id="navbarNav">
      <ul class="navbar-nav ml-auto">
       <li class="nav-item">
        <a class="nav-link" href="/">
         Início
        </a>
       </li>
       <li class="nav-item">
        <a class="nav-link" href="/europeias">
         Europeias
        </a>
       </li>
       <li class="nav-item active">
        <a class="nav-link" href="/legislativas">
         Legislativas
        </a>
       </li>
       <li class="nav-item">
        <a class="nav-link" href="/sobre">
         Sobre
        </a>
       </li>
      </ul>
     </div>
    </div>
   </nav>
  </header>
  <main class="my-3">
   <div class="container-fluid">
    <nav aria-label="breadcrumb" class="mt-1 mb-3">
     <ol class="breadcrumb">
      <li class="breadcrumb-item">
       <a href="/">
        Início
       </a>
      </li>
      <li class="breadcrumb-item">
       <a href="/legislativas">
        Legislativas
       </a>
      </li>
      <li aria-current="page" class="breadcrumb-item active">
       Eleições legislativas, 1980
      </li>
     </ol>
    </nav>
    <div class="dropdown my-1 ml-3 float-right">
     <button aria-expanded="false" aria-haspopup="true" class="btn btn-outline-secondary dropdown-toggle" data-toggle="dropdown" id="dropdownMenuButton" type="button">
      Eleições
     </button>
     <div aria-labelledby="dropdownMenuButton" class="dropdown-menu dropdown-menu-right" style="z-index: 1040;">
      <a class="dropdown-item" href="/legislativas/2024">
       Eleições legislativas, 2024
      </a>
      <a class="dropdown-item" href="/legislativas/2022">
       Eleições legislativas, 2022
      </a>
      <a class="dropdown-item" href="/legislativas/2019">
       Eleições legislativas, 2019
      </a>
      <a class="dropdown-item" href="/legislativas/2015">
       Eleições legislativas, 2015
      </a>
      <a class="dropdown-item" href="/legislativas/2011">
       Eleições legislativas, 2011
      </a>
      <a class="dropdown-item" href="/legislativas/2009">
       Eleições legislativas, 2009
      </a>
      <a class="dropdown-item" href="/legislativas/2005">
       Eleições legislativas, 2005
      </a>
      <a class="dropdown-item" href="/legislativas/2002">
       Eleições legislativas, 2002
      </a>
      <a class="dropdown-item" href="/legislativas/1999">
       Eleições legislativas, 1999
      </a>
      <a class="dropdown-item" href="/legislativas/1995">
       Eleições legislativas, 1995
      </a>
      <a class="dropdown-item" href="/legislativas/1991">
       Eleições legislativas, 1991
      </a>
      <a class="dropdown-item" href="/legislativas/1987">
       Eleições legislativas, 1987
      </a>
      <a class="dropdown-item" href="/legislativas/1985">
       Eleições legislativas, 1985
      </a>
      <a class="dropdown-item" href="/legislativas/1983">
       Eleições legislativas, 1983
      </a>
      <a class="dropdown-item" href="/legislativas/1980">
       Eleições legislativas, 1980
      </a>
      <a class="dropdown-item" href="/legislativas/1979">
       Eleições legislativas, 1979
      </a>
      <a class="dropdown-item" href="/legislativas/1976">
       Eleições legislativas, 1976
      </a>
     </div>
    </div>
    <h1 class="d-inline-block">
     Eleições legislativas, 1980
    </h1>
    <hr/>
    <ul class="nav nav-tabs nav-fill mt-1">
     <li aria-current="page" class="nav-item active">
      <a class="nav-link active" href="/legislativas/1980">
       Resultados
      </a>
     </li>
     <li class="nav-item">
      <a class="nav-link" href="/legislativas/1980/analise">
       Análise
      </a>
     </li>
     <li class="nav-item">
      <a class="nav-link" href="/legislativas/1980/sim">
       Simulações
      </a>
     </li>
     <li class="nav-item">
      <a class="nav-link" href="/legislativas/1980/dados">
       Dados
      </a>
     </li>
     <li class="nav-item">
      <a class="nav-link" href="/legislativas/1980/dist">
       CÍRCULOS
      </a>
     </li>
    </ul>
    <article class="bg-white mb-3 p-4 border border-top-0">
     <p class="lead float-left">
      Eleições para a Assembleia da República realizadas em 5 de Outubro de 1980
     </p>
     <p class="text-right">
      <a aria-controls="legend" aria-expanded="false" class="btn btn-outline-secondary" data-toggle="collapse" href="#legend" role="button">
       Legenda
      </a>
     </p>
     <div class="collapse" id="legend">
      <div class="card border-0">
       <div class="row my-3">
        <div class="col-lg-2">
         <div class="col-content text-lg-right">
          <abbr title="Partido ou coligação">
           Lista
          </abbr>
         </div>
        </div>
        <div class="col-lg-8">
         <div class="col-content">
          <div class="progress my-1 rounded-0 bg-transparent">
           <div aria-valuemax="100%" aria-valuemin="0" aria-valuenow="100%" class="progress-bar" role="progressbar" style="width: 100%; background-color: #888888">
           </div>
          </div>
          % Votos
          <small class="ml-3 text-muted">
           Nº Votos
          </small>
         </div>
        </div>
        <div class="col-lg-2">
         <div class="col-content text-right text-lg-left">
          <p class="h5 mb-1">
           <strong style="color: #888888">
            Mandatos
            <small class="text-muted ml-1">
             / Total
            </small>
           </strong>
          </p>
          <p class="mb-0">
           % Mandatos
          </p>
          <p class="mb-0">
           <span class="badge badge-light text-info ml-2">
            ganho: %V &lt; %M
           </span>
          </p>
          <p class="mb-0">
           <span class="badge badge-light text-danger ml-2">
            perda: %V &gt; %M
           </span>
          </p>
         </div>
        </div>
       </div>
      </div>
     </div>
     <hr>
      <div class="row my-3">
       <div class="col-lg-2">
        <div class="col-content text-lg-right">
         <abbr title="Aliança Democrática">
          AD
         </abbr>
        </div>
       </div>
       <div class="col-lg-8">
        <div class="col-content">
         <div class="progress my-1 rounded-0 bg-transparent">
          <div aria-valuemax="100%" aria-valuemin="0" aria-valuenow="2706667" class="progress-bar" role="progressbar" style="width: 100.00%; background-color: #2A52BE">
          </div>
         </div>
         45.96%
         <small class="ml-3 text-muted">
          2706667 votos
         </small>
        </div>
       </div>
       <div class="col-lg-2">
        <div class="col-content text-right text-lg-left">
         <p class="h5 mb-1">
          <strong style="color:#2A52BE">
           126
           <small class="text-muted ml-1">
            / 250
           </small>
          </strong>
         </p>
         <p class="mb-0">
          50.40%
          <span class="badge badge-light text-info ml-2">
           +4.44%
          </span>
         </p>
        </div>
       </div>
      </div>
      <div class="row my-3">
       <div class="col-lg-2">
        <div class="col-content text-lg-right">
         <abbr title="Frente Republicana e Socialista">
          FRS
         </abbr>
        </div>
       </div>
       <div class="col-lg-8">
        <div class="col-content">
         <div class="progress my-1 rounded-0 bg-transparent">
          <div aria-valuemax="100%" aria-valuemin="0" aria-valuenow="1606198" class="progress-bar" role="progressbar" style="width: 59.34%; background-color: #ff3366">
          </div>
         </div>
         27.28%
         <small class="ml-3 text-muted">
          1606198 votos
         </small>
        </div>
       </div>
       <div class="col-lg-2">
        <div class="col-content text-right text-lg-left">
         <p class="h5 mb-1">
          <strong style="color:#ff3366">
           71
           <small class="text-muted ml-1">
            / 250
           </small>
          </strong>
         </p>
         <p class="mb-0">
          28.40%
          <span class="badge badge-light text-info ml-2">
           +1.12%
          </span>
         </p>
        </div>
       </div>
      </div>
      <div class="row my-3">
       <div class="col-lg-2">
        <div class="col-content text-lg-right">
         <abbr title="Aliança Povo Unido">
          APU
         </abbr>
        </div>
       </div>
       <div class="col-lg-8">
        <div class="col-content">
         <div class="progress my-1 rounded-0 bg-transparent">
          <div aria-valuemax="100%" aria-valuemin="0" aria-valuenow="1009505" class="progress-bar" role="progressbar" style="width: 37.30%; background-color: #ff0000">
          </div>
         </div>
         17.14%
         <small class="ml-3 text-muted">
          1009505 votos
         </small>
        </div>
       </div>
       <div class="col-lg-2">
        <div class="col-content text-right text-lg-left">
         <p class="h5 mb-1">
          <strong style="color:#ff0000">
           41
           <small class="text-muted ml-1">
            / 250
           </small>
          </strong>
         </p>
         <p class="mb-0">
          16.40%
          <span class="badge badge-light text-danger ml-2">
           -0.74%
          </span>
         </p>
        </div>
       </div>
      </div>
      <div class="row my-3">
       <div class="col-lg-2">
        <div class="col-content text-lg-right">
         <abbr title="Partido Social Democrata">
          PSD
         </abbr>
        </div>
       </div>
       <div class="col-lg-8">
        <div class="col-content">
         <div class="progress my-1 rounded-0 bg-transparent">
          <div aria-valuemax="100%" aria-valuemin="0" aria-valuenow="147644" class="progress-bar" role="progressbar" style="width: 5.45%; background-color: #f68a21">
          </div>
         </div>
         2.51%
         <small class="ml-3 text-muted">
          147644 votos
         </small>
        </div>
       </div>
       <div class="col-lg-2">
        <div class="col-content text-right text-lg-left">
         <p class="h5 mb-1">
          <strong style="color:#f68a21">
           8
           <small class="text-muted ml-1">
            / 250
           </small>
          </strong>
         </p>
         <p class="mb-0">
          3.20%
          <span class="badge badge-light text-info ml-2">
           +0.69%
          </span>
         </p>
        </div>
       </div>
      </div>
      <div class="row my-3">
       <div class="col-lg-2">
        <div class="col-content text-lg-right">
         <abbr title="União Democrática Popular ">
          UDP
         </abbr>
        </div>
       </div>
       <div class="col-lg-8">
        <div class="col-content">
         <div class="progress my-1 rounded-0 bg-transparent">
          <div aria-valuemax="100%" aria-valuemin="0" aria-valuenow="83204" class="progress-bar" role="progressbar" style="width: 3.07%; background-color: #ff0000">
          </div>
         </div>
         1.41%
         <small class="ml-3 text-muted">
          83204 votos
         </small>
        </div>
       </div>
       <div class="col-lg-2">
        <div class="col-content text-right text-lg-left">
         <p class="h5 mb-1">
          <strong style="color:#ff0000">
           1
           <small class="text-muted ml-1">
            / 250
           </small>
          </strong>
         </p>
         <p class="mb-0">
          0.40%
          <span class="badge badge-light text-danger ml-2">
           -1.01%
          </span>
         </p>
        </div>
       </div>
      </div>
      <div class="row my-3">
       <div class="col-lg-2">
        <div class="col-content text-lg-right">
         <abbr title="POUS-PST ">
          POUS/PST
         </abbr>
        </div>
       </div>
       <div class="col-lg-8">
        <div class="col-content">
         <div class="progress my-1 rounded-0 bg-transparent">
          <div aria-valuemax="100%" aria-valuemin="0" aria-valuenow="83095" class="progress-bar" role="progressbar" style="width: 3.07%; background-color: #800000">
          </div>
         </div>
         1.41%
         <small class="ml-3 text-muted">
          83095 votos
         </small>
        </div>
       </div>
       <div class="col-lg-2">
        <div class="col-content text-right text-lg-left">
        </div>
       </div>
      </div>
      <div class="row my-3">
       <div class="col-lg-2">
        <div class="col-content text-lg-right">
         <abbr title="Partido Socialista">
          PS
         </abbr>
        </div>
       </div>
       <div class="col-lg-8">
        <div class="col-content">
         <div class="progress my-1 rounded-0 bg-transparent">
          <div aria-valuemax="100%" aria-valuemin="0" aria-valuenow="67081" class="progress-bar" role="progressbar" style="width: 2.48%; background-color: #ff66ff">
          </div>
         </div>
         1.14%
         <small class="ml-3 text-muted">
          67081 votos
         </small>
        </div>
       </div>
       <div class="col-lg-2">
        <div class="col-content text-right text-lg-left">
         <p class="h5 mb-1">
          <strong style="color:#ff66ff">
           3
           <small class="text-muted ml-1">
            / 250
           </small>
          </strong>
         </p>
         <p class="mb-0">
          1.20%
          <span class="badge badge-light text-info ml-2">
           +0.06%
          </span>
         </p>
        </div>
       </div>
      </div>
      <div class="row my-3">
       <div class="col-lg-2">
        <div class="col-content text-lg-right">
         <abbr title="Partido Socialista Revolucionário ">
          PSR
         </abbr>
        </div>
       </div>
       <div class="col-lg-8">
        <div class="col-content">
         <div class="progress my-1 rounded-0 bg-transparent">
          <div aria-valuemax="100%" aria-valuemin="0" aria-valuenow="60496" class="progress-bar" role="progressbar" style="width: 2.24%; background-color: #8b0000">
          </div>
         </div>
         1.03%
         <small class="ml-3 text-muted">
          60496 votos
         </small>
        </div>
       </div>
       <div class="col-lg-2">
        <div class="col-content text-right text-lg-left">
        </div>
       </div>
      </div>
      <div class="row my-3">
       <div class="col-lg-2">
        <div class="col-content text-lg-right">
         <abbr title="Partido Trabalhista ">
          PT
         </abbr>
        </div>
       </div>
       <div class="col-lg-8">
        <div class="col-content">
         <div class="progress my-1 rounded-0 bg-transparent">
          <div aria-valuemax="100%" aria-valuemin="0" aria-valuenow="39408" class="progress-bar" role="progressbar" style="width: 1.46%; background-color: #ff0000">
          </div>
         </div>
         0.67%
         <small class="ml-3 text-muted">
          39408 votos
         </small>
        </div>
       </div>
       <div class="col-lg-2">
        <div class="col-content text-right text-lg-left">
        </div>
       </div>
      </div>
      <div class="row my-3">
       <div class="col-lg-2">
        <div class="col-content text-lg-right">
         <abbr title="Partido Comunista dos Trabalhadores Portugueses ">
          PCTP/MRPP
         </abbr>
        </div>
       </div>
       <div class="col-lg-8">
        <div class="col-content">
         <div class="progress my-1 rounded-0 bg-transparent">
          <div aria-valuemax="100%" aria-valuemin="0" aria-valuenow="35409" class="progress-bar" role="progressbar" style="width: 1.31%; background-color: #ff0000">
          </div>
         </div>
         0.60%
         <small class="ml-3 text-muted">
          35409 votos
         </small>
        </div>
       </div>
       <div class="col-lg-2">
        <div class="col-content text-right text-lg-left">
        </div>
       </div>
      </div>
      <div class="row my-3">
       <div class="col-lg-2">
        <div class="col-content text-lg-right">
         <abbr title="PDC-MIRN/PDP-FN ">
          PDC-MIRN/PDP-FN
         </abbr>
        </div>
       </div>
       <div class="col-lg-8">
        <div class="col-content">
         <div class="progress my-1 rounded-0 bg-transparent">
          <div aria-valuemax="100%" aria-valuemin="0" aria-valuenow="23819" class="progress-bar" role="progressbar" style="width: 0.88%; background-color: #556b2f">
          </div>
         </div>
         0.40%
         <small class="ml-3 text-muted">
          23819 votos
         </small>
        </div>
       </div>
       <div class="col-lg-2">
        <div class="col-content text-right text-lg-left">
        </div>
       </div>
      </div>
      <div class="row my-3">
       <div class="col-lg-2">
        <div class="col-content text-lg-right">
         <abbr title="Centro Democrático Social">
          CDS
         </abbr>
        </div>
       </div>
       <div class="col-lg-8">
        <div class="col-content">
         <div class="progress my-1 rounded-0 bg-transparent">
          <div aria-valuemax="100%" aria-valuemin="0" aria-valuenow="13765" class="progress-bar" role="progressbar" style="width: 0.51%; background-color: #0091dc">
          </div>
         </div>
         0.23%
         <small class="ml-3 text-muted">
          13765 votos
         </small>
        </div>
       </div>
       <div class="col-lg-2">
        <div class="col-content text-right text-lg-left">
        </div>
       </div>
      </div>
      <div class="row my-3">
       <div class="col-lg-2">
        <div class="col-content text-lg-right">
         <abbr title="Partido Democrático do Atlântico ">
          UDA/PDA
         </abbr>
        </div>
       </div>
       <div class="col-lg-8">
        <div class="col-content">
         <div class="progress my-1 rounded-0 bg-transparent">
          <div aria-valuemax="100%" aria-valuemin="0" aria-valuenow="8529" class="progress-bar" role="progressbar" style="width: 0.32%; background-color: #036a84">
          </div>
         </div>
         0.14%
         <small class="ml-3 text-muted">
          8529 votos
         </small>
        </div>
       </div>
       <div class="col-lg-2">
        <div class="col-content text-right text-lg-left">
        </div>
       </div>
      </div>
      <div class="row my-3">
       <div class="col-lg-2">
        <div class="col-content text-lg-right">
         <abbr title="Organização Comunista Marxista-Leninista">
          OCMLP
         </abbr>
        </div>
       </div>
       <div class="col-lg-8">
        <div class="col-content">
         <div class="progress my-1 rounded-0 bg-transparent">
          <div aria-valuemax="100%" aria-valuemin="0" aria-valuenow="3913" class="progress-bar" role="progressbar" style="width: 0.14%; background-color: #ff0000">
          </div>
         </div>
         0.07%
         <small class="ml-3 text-muted">
          3913 votos
         </small>
        </div>
       </div>
       <div class="col-lg-2">
        <div class="col-content text-right text-lg-left">
        </div>
       </div>
      </div>
      <hr/>
      <p class="lead text-center">
       Perfil dos votos
      </p>
      <div class="row my-3">
       <div class="col-lg-2">
        <div class="col-content text-lg-right">
         <p class="mb-1">
          Votos expressos
         </p>
        </div>
       </div>
       <div class="col-lg-8">
        <div class="col-content">
         <div class="progress my-1 rounded-0">
          <div aria-valuemax="6026395" aria-valuemin="0" aria-valuenow="6026395" class="progress-bar progress-bar-striped" role="progressbar" style="width: 100.00%; background-color: #8888cc">
          </div>
         </div>
         <p>
          6026395 votos
         </p>
        </div>
       </div>
       <div class="col-lg-2">
        <div class="col-content text-right text-lg-left">
        </div>
       </div>
      </div>
      <div class="row my-3">
       <div class="col-lg-2">
        <div class="col-content text-lg-right">
         <p class="mb-1">
          Votos válidos
         </p>
        </div>
       </div>
       <div class="col-lg-8">
        <div class="col-content">
         <div class="progress my-1 rounded-0">
          <div aria-valuemax="6026395" aria-valuemin="0" aria-valuenow="5888733" class="progress-bar progress-bar-striped" role="progressbar" style="width: 97.72%; background-color: #88cc88">
          </div>
         </div>
         <p>
          97.72%
          <span class="ml-3 text-muted">
           5888733 votos
          </span>
         </p>
        </div>
       </div>
       <div class="col-lg-2">
        <div class="col-content text-right text-lg-left">
        </div>
       </div>
      </div>
      <div class="row my-3">
       <div class="col-lg-2">
        <div class="col-content text-lg-right">
         Brancos
        </div>
       </div>
       <div class="col-lg-8">
        <div class="col-content">
         <div class="progress my-1 rounded-0">
          <div aria-valuemax="6026395" aria-valuemin="0" aria-valuenow="34522" class="progress-bar progress-bar-striped" role="progressbar" style="width: 0.57%; background-color: #cc88cc">
          </div>
         </div>
         <p>
          0.57%
          <span class="ml-3 text-muted">
           34522 votos
          </span>
         </p>
        </div>
       </div>
       <div class="col-lg-2">
        <div class="col-content text-right text-lg-left">
        </div>
       </div>
      </div>
      <div class="row my-3">
       <div class="col-lg-2">
        <div class="col-content text-lg-right">
         Nulos
        </div>
       </div>
       <div class="col-lg-8">
        <div class="col-content">
         <div class="progress my-1 rounded-0">
          <div aria-valuemax="6026395" aria-valuemin="0" aria-valuenow="103140" class="progress-bar progress-bar-striped" role="progressbar" style="width: 1.71%; background-color: #cc88cc">
          </div>
         </div>
         <p>
          1.71%
          <span class="ml-3 text-muted">
           103140 votos
          </span>
         </p>
        </div>
       </div>
       <div class="col-lg-2">
        <div class="col-content text-right text-lg-left">
        </div>
       </div>
      </div>
     </hr>
    </article>
    <div class="media m-3 float-left">
     <svg class="chart" height="80" style="border-radius: 50%; transform: rotate(-90deg);" width="80">
      <circle class="pie" cx="40" cy="40" fill="#cccccc" r="40" stroke="#8888cc" stroke-dasharray="210.86858532142884, 1803370577.6000001" stroke-width="80">
      </circle>
     </svg>
     <div class="media-body ml-3">
      <p>
       <small>
        Votantes: 6026395
       </small>
       <br/>
       <small>
        Eleitores: 7179023
       </small>
      </p>
      <p>
       <em>
        Abstenção: 16.06%
       </em>
      </p>
     </div>
    </div>
    <p class="lead m-3 text-right">
     Mandatos: 250
    </p>
   </div>
  </main>
  <footer class="text-dark bg-dark py-3 text-center">
   <div class="container-fluid my-3">
    <p class="text-light">
     Construído com
     <a href="_blank">
      Python
     </a>
     ,
     <a href="https://palletsprojects.com/p/flask/" target="_blank">
      Flask
     </a>
     ,
     <a href="https://getbootstrap.com/" target="_blank">
      Bootstrap
     </a>
     e poucas horas de sono.
    </p>
    <p class="text-light">
     Isto é um protótipo sujeito a alterações e publicado sem garantias de qualquer espécie.
    </p>
    <p class="text-light">
     Contacto:
     <a href="mailto:votosemandatos@alus.pt">
      votosemandatos@alus.pt
     </a>
    </p>
   </div>
  </footer>
  <!-- Optional JavaScript -->
  <!-- jQuery first, then Popper.js, then Bootstrap JS -->
  <script crossorigin="anonymous" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" src="https://code.jquery.com/jquery-3.3.1.slim.min.js">
  </script>
  <script crossorigin="anonymous" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js">
  </script>
  <script crossorigin="anonymous" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js">
  </script>
 </body>
</html>