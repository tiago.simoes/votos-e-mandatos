# FLOW

arquitectura geral da aplicação

estrutura:
  UTILS para lidar com ficheiros, importar e exportar
  FRAMES para lidar com dados em formato tabular
  ALLOCATION para aplicar métodos e extrair informação
  LOADER para codificar e descodificar JSON
  PROCESSOR para tratamento e formatação de dados
  PAGES para combinar componentes e criar páginas com dados
  COMPONENTS para transformar dados em HTML
  WEBAPP para visualizar a informação no browser

  EUROPEIAS e LEGISLATIVAS para exemplificar
  cada uma inclui o índice de resultados disponíveis
  cada uma inclui a sequência de operações a realizar

funcionalidade:
  carrega dados a partir de csv
  processa dados e extrai informação
  grava dados tratados e informação em json
  extrai dados e informação de json
  decora e imprime em html

fluxo:
  U   lançar a webapp
      visitar a página de uma categoria

  A   verificar o índice
        init()
        for item in init: assert json
      assegurar presença de json - ou há ou cria
        filename CSV to basename
        basename to filename JSON

      apresentar a lista disponível com resumo

  U   visitar a página duma eleição

  A   assegurar presença de json - ou há ou cria
      apresentar tabela de resultados finais reais
      apresentar análise de resultados
      apresentar comparativo de simulações
