from os import path
import pandas as pd
from utils import csv_to_df
from processor import df_info_extract, df_preprocess, df_process, df_postprocess

# 1) Elenca os ficheiros de resultados disponíveis.

def init():

    dict = {
        'route' : 'europeias_view',
        'basedir' : 'europeias',
        'files' : {
            '2019' : {
                'csv' : 'pe_2019.csv',
                'title' : 'Eleições europeias, 2019'
            },
            '2014' : {
                'csv' : 'pe_2014.csv',
                'title' : 'Eleições europeias, 2014'
            },
            '2009' : {
                'csv' : 'pe_2009.csv',
                'title' : 'Eleições europeias, 2009'
            },
            '2004' : {
                'csv' : 'pe_2004.csv',
                'title' : 'Eleições europeias, 2004'
            },
            '1999' : {
                'csv' : 'pe_1999.csv',
                'title' : 'Eleições europeias, 1999'
            },
            '1994' : {
                'csv' : 'pe_1994.csv',
                'title' : 'Eleições europeias, 1994'
            },
            '1989' : {
                'csv' : 'pe_1989.csv',
                'title' : 'Eleições europeias, 1989'
            },
            '1987' : {
                'csv' : 'pe_1987.csv',
                'title' : 'Eleições europeias, 1987'
            }
        }
    }

    return dict

# 2) Fornece a estrutura de navegação vertical (breadcrumb).

def crumbs(key, page_title, levels=['list']):

    crumbs = [
        ('/', 'index', 'Início'),
    ]

    structure = {
        'list'      : ('/europeias', 'europeias_list', 'Europeias'),
        'view'      : ('/europeias/' + key, 'europeias_view', page_title),
        'analysis'  : ('/europeias/' + key + '/analise', 'europeias_analysis', 'Análise'),
        'sim'       : ('/europeias/' + key + '/sim', 'europeias_sim', 'Simulações'),
        'data'      : ('/europeias/' + key + '/dados', 'europeias_data', 'Dados')
    }

    for level in levels:
        crumbs.append(structure[level])

    return crumbs

# 3) Fornece a estrutura de navegação horizontal (tabs).

# Primeiro nível de navegação
def tabs(key):

    tabs = [
        ('/europeias/' + key, 'europeias_view', 'Resultados'),
        ('/europeias/' + key + '/analise', 'europeias_analysis', 'Análise'),
        ('/europeias/' + key + '/sim', 'europeias_sim', 'Simulações'),
        ('/europeias/' + key + '/dados', 'europeias_data', 'Dados')
    ]

    return tabs

# 4) Cria a estrutura de dados para processamento e devolve os resultados.

def process(dir, file):

    file_path = path.join(dir, file)
    print('\nEUROPEIAS: a processar o ficheiro ' + file_path)

    # esquema geral da informação
    output = {
        'source': {},
        'meta': {},
        'stats': {},
        'total': {}
    }

    # detalhes da origem dos dados
    output['source'] = {
        'dir': dir,
        'file': file,
        'mtime': path.getmtime(file_path)
    }

    # informação no ficheiro
    df, title, comments = csv_to_df(file_path)

    output['source']['title'] = title
    output['source']['comments'] = comments

    # pré-formatação
    output['meta'] = df_info_extract(df)

    col_total = list(df)[-1]
    df.rename(columns = {col_total:'Total'}, inplace=True)

    # métodos a utilizar
    # (m_id, m_init, m_key)
    methods = [
        ('hondt', 1, 'h'),
        ('sl', 2, 'sl'),
        ('dk', 3, 'dk')
    ]

    # pré-processamento dos dados
    stats, df_i, df_v = df_preprocess(df)

    output['total']['stats'] = stats

    # processamento dos dados
    s_alloc, df_v = df_process(df_v, stats, methods)

    output['total']['alloc'] = s_alloc

    # pós-processamento dos dados
    json_i, json_v = df_postprocess(df_i, df_v)

    output['total']['df_i'] = json_i
    output['total']['df_v'] = json_v

    return output

# Interface de linha de comando
# Uso: python europeias.py

if __name__ == '__main__':

    print('+--------------+')
    print('| europeias.py |')
    print('+--------------+')

    print('\nContém funções específicas do processamento de resultados das europeias.\n')

    print('1) Elenca os ficheiros de resultados disponíveis.')
    print('2) Fornece a estrutura de navegação vertical (breadcrumb).')
    print('3) Fornece a estrutura de navegação horizontal (tabs).')
    print('4) Cria a estrutura de dados para processamento e devolve os resultados.')

    print('\nNenhuma demonstração disponível.\n')
