import pandas as pd
from math import ceil
from pick import pick

# Método das médias mais altas - generalizado
# HAM: Highest Averages Method

def alloc_ham(df_v, col_id, col_v, s_sum, col_s = 's', init = 1):

    df_v[col_s] = 0
    s_iter = 0
    s_alloc = []

    while s_iter < s_sum:
        # método generalizado
        # init = 1 : método de d'Hondt
        # init = 2 : método de Sainte-Laguë
        # init = 3 : método dinamarquês
        df_v['q'] = df_v[col_v] / (1 + (df_v[col_s] * init))

        # em caso de empate, preferir quem tem menos mandatos
        df_aux = df_v.sort_values(by=[col_s])
        # localizar o maior quociente
        max_index = df_aux['q'].idxmax()
        # alocar o mandato
        df_v.loc[max_index,[col_s]] += 1
        s_iter += 1

        # registar a ordem de alocação
        c_alloc = df_v.loc[max_index,[col_id]].values[0]
        s_count = df_v.loc[max_index,[col_s]].values[0]
        q_alloc = df_v.loc[max_index,['q']].values[0]
        s_alloc.append((c_alloc, int(s_count), float(q_alloc)))

    df_v.pop('q')

    return s_alloc

def next_ham(df_v, col_id, col_v, col_s, s_alloc, col_diff = 'v_diff', init = 1):

    # extrair última alocação
    c_last, s_count, q_last = s_alloc[-1]
    # calcular os quocientes da próxima iteração
    # init = 1 : método de d'Hondt
    # init = 2 : método de Sainte-Laguë
    # init = 3 : método dinamarquês
    df_v['q'] = df_v[col_v] / (1 + (df_v[col_s] * init))
    # calcular as diferenças para o último quociente
    df_v['q_diff'] = q_last - df_v['q']
    # calcular o número de votos para cobrir a diferença
    df_v[col_diff] = (df_v['q_diff'] * (1 + (df_v[col_s] * init)))
    # arrendondar para o inteiro imediatamente acima
    df_v[col_diff] = df_v[col_diff].apply(lambda x: ceil(x))
    # diferença de zero implica precisar de mais um para eleger
    i_tied = df_v[df_v[col_diff] < 1].index.values
    df_v.loc[i_tied,[col_diff]] += 1
    # ignorar o último eleito
    i_last = df_v[df_v[col_id] == c_last].index.values
    df_v.loc[i_last,[col_diff]] = 0

    print('\nDEBUG')
    print(df_v[[col_id,
                col_v,
                (col_v + '_pc'),
                col_s,
                (col_s + '_pc'),
                (col_s + '_dpc'),
                (col_v + '/' + col_s),
                ('0' + col_s),
                'q',
                'q_diff',
                (col_s + '_diff')]])
    print('\nEND')
    df_v.pop('q')
    df_v.pop('q_diff')

# Funções de demonstração

def wasted(df_v, col_v, col_s):
    df_wasted = df_v[df_v[col_s] == 0]
    v_wasted = df_wasted[col_v].sum()

    return v_wasted

def alloc_agg(d_alloc):

    # Agregar as alocações
    s_agg = {}
    for d in d_alloc:
        # Agregar por círculo
        d_agg = {}
        for line in d_alloc[d]:
            c, s, q = line
            d_agg[c] = s
        # Somar ao total
        for c in d_agg:
            if c in s_agg:
                s_agg[c] += d_agg[c]
            else:
                s_agg[c] = d_agg[c]

    return s_agg

def demo_gen_df():
    df = pd.DataFrame(
            [['A', 100000],
            ['B', 80000],
            ['C', 30000],
            ['D', 20000]],
            index = [0,1,2,3],
            columns = ['Lista','Votos'])

    print('\nTabela de resultados:\n')
    print(df)

    s_sum = 8

    print('\nNúmero de mandatos a atribuir: ' + str(s_sum))

    input('\nPrima ENTER para continuar...')

    return df, s_sum

def demo_hondt():
    print('+---------------------+')
    print('| Método de d\'Hondt |')
    print('+---------------------+')

    print('\nAplicação do método de d\'Hondt passo a passo.')
    print('Esta demonstração utiliza um DataFrame simplificado.')
    print('Origem: https://en.wikipedia.org/wiki/D\'Hondt_method')

    input('\nPrima ENTER para continuar...')

    df_v, s_sum = demo_gen_df()

    df_v['Mandatos'] = 0
    s_iter = 0
    s_alloc = []

    while s_iter < s_sum:
        # método de d'Hondt
        df_v['Quociente'] = df_v['Votos'] / (1 + df_v['Mandatos'])
        # em caso de empate, atribuír a quem tem menos mandatos
        df_aux = df_v.sort_values(by=['Mandatos'])
        max_index = df_aux['Quociente'].idxmax()
        # descrever
        step = str(s_iter + 1)
        print('\n--> ' + step + 'º passo:')
        print('\nTabela actualizada:\n')
        print(df_v)
        q_alloc = df_v.loc[max_index,['Quociente']].values[0]
        print('\nQuociente mais alto: ' + str(q_alloc))
        c_alloc = df_v.loc[max_index,['Lista']].values[0]
        print('\nLista correspondente: ' + str(c_alloc))
        # atribuir
        df_v.loc[max_index,['Mandatos']] += 1
        s_count = df_v.loc[max_index,['Mandatos']].values[0]
        print('\n' + str(c_alloc) + ' conquista o ' + str(s_iter + 1) + 'º mandato do círculo.')
        print('\n' + str(c_alloc) + ' conquista o seu ' + str(s_count) + 'º mandato.')
        # registar
        s_alloc.append((c_alloc, s_count, q_alloc))
        s_iter += 1

        input('\nPrima ENTER para continuar...')

    print('\nTabela final:\n')
    df_v.pop('Quociente')
    print(df_v)

def tour():

    print('+---------------+')
    print('| allocation.py |')
    print('+---------------+')

    print('\nEste módulo contém funções de alocação de mandatos.')
    print('Esta demonstração utiliza um DataFrame simplificado.')
    print('Origem: https://en.wikipedia.org/wiki/D\'Hondt_method')

    input('\nPrima ENTER para continuar...')

    df, s_sum = demo_gen_df()

    s_alloc = alloc_ham(df, 'Lista', 'Votos', s_sum, 'Mandatos')
    print('\nSequência de alocação:\n')
    for c, s, q in s_alloc:
        print(c + ' elege o seu ' + str(s) + 'º deputado com quociente ' + str(int(round(q))))

    print('\nDataFrame actualizado:\n')
    print(df)

    v_diff = next_ham(df, 'Lista', 'Votos', 'Mandatos', s_alloc)
    print('\nDiferenças de votos:\n')
    for c, s, v in v_diff:
        print(c + ' precisaria de mais ' + str(v) + ' votos para eleger o seu ' + str(s + 1) + 'º deputado')

    v_wasted = wasted(df, 'Votos', 'Mandatos')
    print('\nVotos desperdiçados: ' + str(v_wasted))
    print('\nVotos desperdiçados são votos válidos em listas que não elegem.')

    print('\n====================')
    print('Fim da demonstração.')

def launch():

    title = 'VOTOS: allocation.py\nSeleccione uma opção:'
    menu_options = ['Sair',
                    'Demonstração',
                    'Método de d\'Hondt']

    menu_item, index = pick(menu_options, title)

    if index == 1:
        tour()

    if index == 2:
        demo_hondt()

    return index

# Interface de linha de comando
# Uso: python allocation.py

if __name__ == '__main__':
    index = -1
    while index != 0:
        index = launch()
        if index == 0:
            print('\nObrigado e volte sempre!')
        else:
            input('\nPrima ENTER para regressar ao menu.')
