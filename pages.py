import pandas as pd
from flask import url_for, render_template, Markup
from frames import df_new
from components import df_info_abbr, df_to_table, df_compress_null, df_decorate_diff, df_decorate_progress, stats_html, stats_stack, wasted_stack, method_stack

# Página de resultados segundo o modelo utilizado pelo MAI nas legislativas recentes

def results_table(title, df_v, col_s, df_i, stats, dict_meta):
    results_markup = '<p class="lead float-left">' + title + '</p>'

    legend_title = "Legenda"
    legend_id = "legend"
    # formatar legenda
    col_header = '<abbr title="Partido ou coligação">Lista</abbr>'
    col_body = []
    col_body.append(render_template('components/progress.html', hex='#888888',value='100%', pc='100%', max='100%', transparent=True))
    col_body.append('% Votos <small class="ml-3 text-muted">Nº Votos</small>')
    col_footer = []
    col_footer.append('<p class="h5 mb-1"><strong style="color: #888888">Mandatos<small class="text-muted ml-1">/ Total</small></strong></p>')
    col_footer.append('<p class="mb-0">% Mandatos</p>')
    col_footer.append('<p class="mb-0"><span class="badge badge-light text-info ml-2">ganho: %V < %M</span></p>')
    col_footer.append('<p class="mb-0"><span class="badge badge-light text-danger ml-2">perda: %V > %M</span></p>')

    legend_body = render_template('components/results-table.html', col_header=col_header, col_body=col_body, col_footer=col_footer)
    legend_markup = render_template('components/collapse.html', classes='text-right', title=legend_title, id=legend_id, body=legend_body)

    results_markup += legend_markup
    results_markup += '<hr />'

    v_cols = ['Lista','v','v_pc',col_s,col_s + '_pc',col_s + '_dpc']
    df_display_v = df_v[v_cols].copy()
    df_display_i = df_i[['Lista','i','i_pc']].copy()

    # recuperar os códigos de cor
    dict_hex = {}
    for key in dict_meta:
        dict_hex[key] = dict_meta[key][1]
    df_new(df_display_v, 'Lista', dict_hex, col_dest = 'HEX')

    # juntos os nomes dos partidos como título de abreviatura
    df_info_abbr(df_display_v, 'Lista', dict_meta, circle = False)

    # retirar o valor máximo para normalizar as barras de progresso
    v_pc_max = df_display_v['v_pc'].max()

    # formatar os números de votos e mandatos segundo o visual clássico
    for index, row in df_display_v.iterrows():
        # formatação dos votos
        pc_norm = '{:.2%}'.format((row['v_pc'] / v_pc_max))
        col_body = []
        col_body.append(render_template('components/progress.html', hex=row['HEX'],value=row['v'], pc=pc_norm, max='100%', transparent=True))
        col_body_legend = '\n' + '{:.2%}'.format(row['v_pc']) + '<small class="ml-3 text-muted">' + str(row['v']) + ' votos</small>'
        col_body.append(col_body_legend)

        # formatação dos mandatos
        if row[col_s] == 0:
            col_footer = []
        else:
            col_footer = []
            col_footer.append('<p class="h5 mb-1"><strong style="color:' + str(row['HEX']) + '">' + str(row[col_s]) + '<small class="text-muted ml-1">/ ' + str(stats['s']) + '</small></strong></p>')
            if row[col_s + '_dpc'] > 0:
                col_footer.append('<p class="mb-0">' + '{:.2%}'.format(row[col_s + '_pc']) + '<span class="badge badge-light text-info ml-2">+' + '{:.2%}'.format(row[col_s + '_dpc']) + '</span></p>')
            else:
                col_footer.append('<p class="mb-0">' + '{:.2%}'.format(row[col_s + '_pc']) + '<span class="badge badge-light text-danger ml-2">' + '{:.2%}'.format(row[col_s + '_dpc']) + '</span></p>')

        # juntar os elementos da linha
        results_markup += render_template('components/results-table.html', col_header=row['Lista'], col_body=col_body, col_footer=col_footer)

    results_markup += '<hr />'

    # adicionar os detalhes de votos expressos e inválidos
    results_markup += '<p class="lead text-center">Perfil dos votos</p>'

    col_header = '<p class="mb-1">Votos expressos</p>'
    col_body = render_template('components/progress.html', hex='#8888cc',value=stats['vc_sum'], pc='{:.2%}'.format(1), max=stats['vc_sum'], striped=True)
    col_body += '<p>' + str(stats['vc_sum']) + ' votos</p>'
    results_markup += render_template('components/results-table.html', col_header=col_header, col_body=col_body)

    col_header = '<p class="mb-1">Votos válidos</p>'
    col_body = render_template('components/progress.html', hex='#88cc88',value=stats['v_sum'], pc='{:.2%}'.format((stats['v_sum'] / stats['vc_sum'])), max=stats['vc_sum'], striped=True)
    col_body += '<p>' + '{:.2%}'.format((stats['v_sum'] / stats['vc_sum'])) + '<span class="ml-3 text-muted">' + str(stats['v_sum']) + ' votos</span></p>'
    results_markup += render_template('components/results-table.html', col_header=col_header, col_body=col_body)

    for index, row in df_display_i.iterrows():
        col_body = render_template('components/progress.html', hex='#cc88cc',value=row['i'], pc='{:.2%}'.format(row['i_pc']), max=stats['vc_sum'], striped=True)
        col_body += '<p>' + '{:.2%}'.format(row['i_pc']) + '<span class="ml-3 text-muted">' + str(row['i']) + ' votos</span></p>'
        results_markup += render_template('components/results-table.html', col_header=row['Lista'], col_body=col_body)

    return results_markup

def compressed_table(df_v, col_s, stats, dict_meta, col_ref = 'h'):

    results_markup = ''

    v_cols = ['Lista',
                'v',
                'v_pc']

    if col_s != col_ref:
        v_cols.append(col_ref)
        v_cols.append(col_s)
        v_cols.append(col_s + '-' + col_ref)
    else:
        v_cols.append(col_s)

    v_cols.append(col_s + '_pc')
    v_cols.append(col_s + '_dpc')

    df_aux = df_v[v_cols].copy()

    # juntar os nomes dos partidos como título de abreviatura
    df_info_abbr(df_aux, 'Lista', dict_meta, circle = False)

    # comprimir a tabela agregando linhas com parâmetro nulo
    df_compressed = df_compress_null(df_aux,'v',col_s)

    results_markup += Markup(df_to_table(df_compressed, v_cols))

    return results_markup

def dist_table(d_split):
    d_markup = '<div class="row">'
    for d in d_split:
        d_markup += '<div class="col-md-4">'

        # para template
        d_markup += '<div class="card mb-3">'
        d_markup += '<div class="card-header">' + d + '</div>'
        d_markup += '<div class="card-body">'
        stats = d_split[d]['stats']
        waste_sum = d_split[d]['alloc']['hondt']['waste_sum']
        d_markup += '<p class="">Mandatos: ' + str(stats['s']) + '</p>'
        d_markup += '<hr />'
        d_markup += wasted_stack(stats, waste_sum)
        d_markup += '<hr />'
        d_markup += stats_stack(stats)
        d_markup += '</div>'
        d_markup += '</div>'
        #

        d_markup += '</div>'
    d_markup += '</div>'

    return d_markup

def analysis_single(df_v, col_s, stats, seq_alloc, dict_meta):
    markup = ''

    # Comparação com barras cumulativas
    col_dict = {
                'v_pc': '% Votos',
                'h_pc': '% Mandatos: Método de d\'Hondt',
                }

    for key, value in col_dict.items():
        df_compressed = df_compress_null(df_v, key)
        markup += Markup(method_stack(df_compressed, key, dict_meta, title=value))

    markup += Markup('<hr />')

    # Formatação
    df_info_abbr(df_v, 'Lista', dict_meta)

    df_display = df_v.copy()
    df_display.rename(columns = {'v':'Votos',
                                'v_pc':'% Votos',
                                col_s:'Mandatos',
                                (col_s + '_pc'):'% Mandatos',
                                (col_s + '_dpc'):'Desvio',
                                ('v/' + col_s):'Votos por mandato',
                                (col_s + '_diff'):'Diferença',
                                (col_s + '_ratio'):'Proporção'}, inplace=True)

    # Desvios
    df_compressed = df_compress_null(df_display,'Mandatos')
    # Decorar os valores das diferenças
    df_decorate_diff(df_compressed,'Desvio')

    data_cols = ['Lista','Votos','% Votos','Mandatos','% Mandatos','Desvio']
    markup += Markup(render_template('content/section-dpc.html', markup=df_to_table(df_compressed, data_cols)))

    # Sequência
    for item in seq_alloc:
        if item[0] in dict_meta:
            long, hex = dict_meta[item[0]]
            prefix = '<svg class=\"mr-3\" height=\"20\" width=\"20\"><circle cx=\"10\" cy=\"10\" r=\"8\" fill=\"' + str(hex) + '\" /></svg>'
            prefix += '<abbr title="' + long + '">'
            suffix = '</abbr>'
            item[0] = prefix + item[0] + suffix

    markup += Markup(render_template('content/sequence-list.html', list=seq_alloc))

    # Próximo
    next_alloc = []
    df_next = df_display[df_display['Diferença'] > 0].sort_values('Proporção')
    df_display_next = df_next[df_next['Proporção'] < 1]
    for index, row in df_display_next.iterrows():
        c = row['Lista']
        diff = str(row['Diferença'])
        ratio = '{:,.2%}'.format(row['Proporção'])
        next = str(row['Mandatos'] + 1)
        next_alloc.append([c,diff,ratio,next])

    markup += Markup(render_template('content/next-list.html', list=next_alloc))

    return markup

def analysis_sum(df_v, col_s, stats, dict_meta):
    markup = ''

    # Comparação com barras cumulativas
    col_dict = {
                'v_pc': '% Votos',
                'h_sum_pc': '% Mandatos: Método de d\'Hondt',
                }

    for key, value in col_dict.items():
        df_compressed = df_compress_null(df_v, key)
        markup += Markup(method_stack(df_compressed, key, dict_meta, title=value))

    markup += Markup('<hr />')

    df_info_abbr(df_v, 'Lista', dict_meta)

    df_display = df_v.copy()
    waste_col_header = '<strong class="text-danger">% Desperdiçados</strong> vs <strong class="text-primary">% Úteis</strong>'
    df_display.rename(columns = {'v':'Votos',
                                'v_pc':'% Votos',
                                col_s:'Mandatos',
                                (col_s + '_pc'):'% Mandatos',
                                (col_s + '_dpc'):'Desvio',
                                ('v/' + col_s):'Votos por mandato',
                                ('0' + col_s):'Desperdiçados',
                                ('0' + col_s + '_ratio'):waste_col_header}, inplace=True)

    # Desvios
    df_compressed = df_compress_null(df_display,'Mandatos')
    # Decorar os valores das diferenças
    df_decorate_diff(df_compressed,'Desvio')

    data_cols = ['Lista','Votos','% Votos','Mandatos','% Mandatos','Desvio']
    markup += Markup(render_template('content/section-dpc.html', markup=df_to_table(df_compressed, data_cols)))

    # Votos por mandato
    data_cols = ['Lista','Votos','Mandatos','Votos por mandato']
    markup += Markup(render_template('content/section-vps.html', markup=df_to_table(df_display[df_display['Mandatos'] > 0].sort_values('Votos por mandato', ascending=True), data_cols)))

    # Votos desperdiçados
    data_cols = ['Lista','Votos','Mandatos','Desperdiçados',waste_col_header]
    # Decorar os valores dos desperdícios
    df_decorate_progress(df_display, waste_col_header)
    markup += Markup(render_template('content/section-waste.html', markup=df_to_table(df_display.sort_values('Votos', ascending=False), data_cols)))

    return markup

def methods_sim(df_v, dict_meta, m_ind):
    markup = ''

    # Comparação com barras cumulativas
    col_dict = {
                'v_pc': '% Votos',
                'h_pc': '% Mandatos: Método de d\'Hondt',
                'sl_pc': '% Mandatos: Método de Sainte-Laguë',
                'dk_pc': '% Mandatos: Método Dinamarquês'
                }

    for key, value in col_dict.items():
        df_compressed = df_compress_null(df_v, key)
        markup += Markup(method_stack(df_compressed, key, dict_meta, title=value))

    markup += Markup('<hr />')

    # Tabela de alocações por método utilizado
    df_v_alloc = df_v[['Lista','v','h','sl','dk']][df_v['h'] + df_v['sl'] + df_v['dk'] > 0].copy()
    df_info_abbr(df_v_alloc, 'Lista', dict_meta)
    df_v_alloc.rename(columns = {'v':'Votos',
                                'h':'d\'Hondt',
                                'sl':'Sainte-Laguë',
                                'dk':'Dinamarquês'}, inplace=True)
    data_cols = list(df_v_alloc)
    table_markup = Markup(df_to_table(df_v_alloc, data_cols))

    markup += Markup(render_template('content/section-sim-s.html', markup=table_markup))

    # Tabela de desvios por método utilizado
    df_compressed = df_compress_null(df_v,'dk')
    df_v_dpc = df_compressed[['Lista','h_dpc','sl_dpc','dk_dpc']]
    df_info_abbr(df_v_dpc, 'Lista', dict_meta)
    df_decorate_diff(df_v_dpc,'h_dpc')
    df_decorate_diff(df_v_dpc,'sl_dpc')
    df_decorate_diff(df_v_dpc,'dk_dpc')
    df_v_dpc.rename(columns = {'h_dpc':'d\'Hondt',
                                'sl_dpc':'Sainte-Laguë',
                                'dk_dpc':'Dinamarquês'}, inplace=True)
    data_cols = list(df_v_dpc)
    table_markup = Markup(df_to_table(df_v_dpc, data_cols))

    markup += Markup(render_template('content/section-sim-dpc.html', markup=table_markup))

    # Tabela de indicadores dos métodos utilizados
    df_ind = pd.DataFrame(
            [['Desvios acumulados', '{:.2%}'.format(m_ind[0][0]), '{:.2%}'.format(m_ind[1][0]), '{:.2%}'.format(m_ind[2][0])],
            ['Votos desperdiçados', int(m_ind[0][1]), int(m_ind[1][1]), int(m_ind[2][1])],
            ['Troca de lugares', m_ind[0][2], m_ind[1][2], m_ind[2][2]]],
            index = [0,1,2],
            columns = ['Indicador','d\'Hondt','Sainte-Laguë','Dinamarquês'])

    data_cols = list(df_ind)
    table_markup = Markup(df_to_table(df_ind, data_cols))

    markup += Markup(render_template('content/section-sim-ind.html', markup=table_markup, show=True))

    return markup

def methods_sum(df_v, dict_meta, m_ind):
    markup = ''

    # Comparação com barras cumulativas
    col_dict = {
                'v_pc': '% Votos',
                'h_sum_pc': '% Mandatos: Método de d\'Hondt',
                'sl_sum_pc': '% Mandatos: Método de Sainte-Laguë',
                'dk_sum_pc': '% Mandatos: Método Dinamarquês'
                }

    for key, value in col_dict.items():
        df_compressed = df_compress_null(df_v, key)
        markup += Markup(method_stack(df_compressed, key, dict_meta, title=value))

    markup += Markup('<hr />')

    # Tabela de alocações por método utilizado
    df_v_agg = df_v[['Lista','v','h_sum','sl_sum','dk_sum']][df_v['h_sum'] + df_v['sl_sum'] + df_v['dk_sum'] > 0].copy()
    df_info_abbr(df_v_agg, 'Lista', dict_meta)
    df_v_agg.rename(columns = {'v':'Votos',
                                'h_sum':'d\'Hondt',
                                'sl_sum':'Sainte-Laguë',
                                'dk_sum':'Dinamarquês'}, inplace=True)
    data_cols = list(df_v_agg)
    table_markup = Markup(df_to_table(df_v_agg, data_cols))

    markup += Markup(render_template('content/section-sim-s.html', markup=table_markup))

    # Tabela de desvios por método utilizado
    df_compressed = df_compress_null(df_v,'dk_sum')
    df_v_dpc = df_compressed[['Lista','h_sum_dpc','sl_sum_dpc','dk_sum_dpc']]
    df_info_abbr(df_v_dpc, 'Lista', dict_meta)
    df_decorate_diff(df_v_dpc,'h_sum_dpc')
    df_decorate_diff(df_v_dpc,'sl_sum_dpc')
    df_decorate_diff(df_v_dpc,'dk_sum_dpc')
    df_v_dpc.rename(columns = {'h_sum_dpc':'d\'Hondt',
                                'sl_sum_dpc':'Sainte-Laguë',
                                'dk_sum_dpc':'Dinamarquês'}, inplace=True)
    data_cols = list(df_v_dpc)
    table_markup = Markup(df_to_table(df_v_dpc, data_cols))

    markup += Markup(render_template('content/section-sim-dpc.html', markup=table_markup))

    # Tabela de indicadores dos métodos utilizados
    df_ind = pd.DataFrame(
            [['Desvios acumulados', '{:.2%}'.format(m_ind[0][0]), '{:.2%}'.format(m_ind[1][0]), '{:.2%}'.format(m_ind[2][0])],
            ['Votos desperdiçados', int(m_ind[0][1]), int(m_ind[1][1]), int(m_ind[2][1])],
            ['Troca de lugares', m_ind[0][2], m_ind[1][2], m_ind[2][2]]],
            index = [0,1,2],
            columns = ['Indicador','d\'Hondt','Sainte-Laguë','Dinamarquês'])

    data_cols = list(df_ind)
    table_markup = Markup(df_to_table(df_ind, data_cols))

    markup += Markup(render_template('content/section-sim-ind.html', markup=table_markup, show=True))

    markup += Markup('<h2>Simulações para um círculo único nacional</h2>')

    markup += Markup('<hr />')

    # Comparação com barras cumulativas
    col_dict = {
                'v_pc': '% Votos',
                'h_pc': '% Mandatos: Método de d\'Hondt',
                'sl_pc': '% Mandatos: Método de Sainte-Laguë',
                'dk_pc': '% Mandatos: Método Dinamarquês'
                }

    for key, value in col_dict.items():
        df_compressed = df_compress_null(df_v, key)
        markup += Markup(method_stack(df_compressed, key, dict_meta, title=value))

    markup += Markup('<hr />')

    # Tabela de alocações por método utilizado - CÍRCULO ÚNICO
    df_v_alloc = df_v[['Lista','v','h','sl','dk']][df_v['h'] + df_v['sl'] + df_v['dk'] > 0].copy()
    df_info_abbr(df_v_alloc, 'Lista', dict_meta)
    df_v_alloc.rename(columns = {'v':'Votos',
                                'h':'d\'Hondt',
                                'sl':'Sainte-Laguë',
                                'dk':'Dinamarquês'}, inplace=True)
    data_cols = list(df_v_alloc)
    table_markup = Markup(df_to_table(df_v_alloc, data_cols))

    markup += Markup(render_template('content/section-sim-s.html', markup=table_markup, section_id='msa'))

    # Tabela de desvios por método utilizado - CÍRCULO ÚNICO
    df_compressed = df_compress_null(df_v,'dk')
    df_v_dpc = df_compressed[['Lista','h_dpc','sl_dpc','dk_dpc']]
    df_info_abbr(df_v_dpc, 'Lista', dict_meta)
    df_decorate_diff(df_v_dpc,'h_dpc')
    df_decorate_diff(df_v_dpc,'sl_dpc')
    df_decorate_diff(df_v_dpc,'dk_dpc')
    df_v_dpc.rename(columns = {'h_dpc':'d\'Hondt',
                                'sl_dpc':'Sainte-Laguë',
                                'dk_dpc':'Dinamarquês'}, inplace=True)
    data_cols = list(df_v_dpc)
    table_markup = Markup(df_to_table(df_v_dpc, data_cols))

    markup += Markup(render_template('content/section-sim-dpc.html', markup=table_markup, section_id='mdpca'))

    # Tabela de indicadores dos métodos utilizados
    df_ind = pd.DataFrame(
            [['Desvios acumulados', '{:.2%}'.format(m_ind[3][0]), '{:.2%}'.format(m_ind[4][0]), '{:.2%}'.format(m_ind[5][0])],
            ['Votos desperdiçados', int(m_ind[3][1]), int(m_ind[4][1]), int(m_ind[5][1])],
            ['Troca de lugares', m_ind[3][2], m_ind[4][2], m_ind[5][2]]],
            index = [0,1,2],
            columns = ['Indicador','d\'Hondt','Sainte-Laguë','Dinamarquês'])

    data_cols = list(df_ind)
    table_markup = Markup(df_to_table(df_ind, data_cols))

    markup += Markup(render_template('content/section-sim-ind.html', markup=table_markup, section_id='inda', show=True))

    return markup

# Interface de linha de comando
# Uso: python pages.py

if __name__ == '__main__':

    print('+----------+')
    print('| pages.py |')
    print('+----------+')

    print('\nContém conjuntos de componentes para formar páginas HTML.\n')

    print('\nNenhuma demonstração disponível.\n')
