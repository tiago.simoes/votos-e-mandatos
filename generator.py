from os import scandir, path, makedirs, unlink
from bs4 import BeautifulSoup

if not path.exists('build'):
    makedirs('build')

def print_static(html_str, path_args):
    soup = BeautifulSoup(html_str, 'html.parser')
    dest_path = 'build'
    for arg in path_args:
        dest_path = path.join(dest_path,arg)
        if not path.exists(dest_path):
            makedirs(dest_path)
    html_path = path.join(dest_path,'index.html')
    file = open(html_path,'w+')
    file.write(soup.prettify())
    print('GENERATOR: página gravada como ' + html_path + '\n')
