from flask import url_for, render_template, Markup

# introduce metadata

def df_info_abbr(df, col_id, dict_meta, circle = True):

    for key in dict_meta:
        long, hex = dict_meta[key]
        if circle:
            prefix = '<svg class=\"mr-3\" height=\"20\" width=\"20\"><circle cx=\"10\" cy=\"10\" r=\"8\" fill=\"' + str(hex) + '\" /></svg>'
        else:
            prefix = ''
        prefix += '<abbr title="' + long + '">'
        suffix = '</abbr>'

        c_index = df[df[col_id] == key].index.values
        df.loc[c_index,[col_id]] = prefix + df.loc[c_index,[col_id]] + suffix

def df_info_progress(df, col_id, dict_meta, circle = True):

    for key in dict_meta:
        long, hex = dict_meta[key]
        if circle:
            prefix = '<svg class=\"mr-3\" height=\"20\" width=\"20\"><circle cx=\"10\" cy=\"10\" r=\"8\" fill=\"' + str(hex) + '\" /></svg>'
        else:
            prefix = ''
        prefix += '<abbr title="' + long + '">'
        suffix = '</abbr>'

        c_index = df[df[col_id] == key].index.values
        df.loc[c_index,[col_id]] = prefix + df.loc[c_index,[col_id]] + suffix

# tables

def df_to_table(df, data_cols, header=True):

    df_html = df.to_html(columns=data_cols, header=header, index=False, float_format='{:,.2%}'.format, justify='unset', classes='table', escape=False, border=0)

    return render_template('components/card.html', body=df_html, padded=False)

def df_compress_null(df_src, col_param, col_id = 'Lista', row_id = 'Outros'):

    df_compressed = df_src[df_src[col_param] > 0].copy()
    df_rest = df_src[df_src[col_param] == 0].copy()

    # adicionar uma linha vazia
    new_row = {}
    cols = list(df_compressed)
    for col in cols:
        # índice da linha
        if col == col_id:
            new_row[col] = row_id
        # colunas a ignorar
        elif col in [(col_param + '_pc'), 'v/h','v/sl','v/dk']:
            new_row[col] = ''
        # default: somatório da coluna
        else:
            new_row[col] = df_rest[col].sum()

    df_compressed = df_compressed.append(new_row, ignore_index=True)

    return df_compressed

def df_decorate_diff(df_src, col_diff):

    df_aux = df_src[[col_diff]].copy()

    prefix_p = '<strong class="text-info">+'
    prefix_n = '<strong class="text-danger">'
    suffix = '</strong>'

    for index, row in df_aux.iterrows():
        if row[col_diff] > 0:
            df_src.loc[index,[col_diff]] = prefix_p + '{:.2%}'.format(row[col_diff]) + suffix
        else:
            df_src.loc[index,[col_diff]] = prefix_n + '{:.2%}'.format(row[col_diff]) + suffix

    del df_aux

def df_decorate_progress(df_src, col_progress):

    df_aux = df_src[[col_progress]].copy()
    stack = []
    max = '100%'

    for index, row in df_aux.iterrows():
        waste = '{:.2%}'.format(row[col_progress])
        valid = '{:.2%}'.format(1 - row[col_progress])
        # width, style, value, start, max
        stack = [
            (waste, 'danger', waste, 0, max),
            (valid, 'primary', valid, waste, max)
        ]
        df_src.loc[index,[col_progress]] = Markup(render_template('components/progress-stack.html', items=stack, legend=True).strip())

    del df_aux

# single components

def menu_html(menu_title, dict, route, component = 'list_group'):
    url_list = []
    for item in dict:
        title = dict[item]['title']
        url = url_for(route, key=item)
        url_list.append((url, title))

    if component == 'dropdown':
        return render_template('components/dropdown.html', header=menu_title,  items=url_list)
    else:
        return render_template('components/list-group.html', header=menu_title,  items=url_list)

def d_menu_html(menu_title, key, dict, route, component = 'list_group'):
    url_list = []
    for item in dict:
        title = Markup(item)
        url = url_for(route, key=key, d_key=dict[item])
        url_list.append((url, title))

    if component == 'dropdown':
        return render_template('components/dropdown.html', header=Markup(menu_title),  items=url_list)
    else:
        return render_template('components/list-group.html', header=Markup(menu_title),  items=url_list)

def k_menu_html(menu_title, d_key, dict, route, component = 'list_group'):
    url_list = []
    for item in dict:
        title = dict[item]['title']
        # estas excepções deviam ser colocadas noutro lado
        if item == '1976':
            if d_key == 'madeira':
                d_key = 'funchal'
        else:
            if d_key == 'funchal':
                d_key = 'madeira'
            elif d_key == 'angra-do-heroismo':
                d_key = 'acores'
            elif d_key == 'horta':
                d_key = 'acores'
            elif d_key == 'ponta-delgada':
                d_key = 'acores'

        url = url_for(route, key=item, d_key=d_key)
        url_list.append((url, title))

    if component == 'dropdown':
        return render_template('components/dropdown.html', header=Markup(menu_title),  items=url_list)
    else:
        return render_template('components/list-group.html', header=Markup(menu_title),  items=url_list)

def stats_html(stats):
    stats_markup = ''

    chart_body = ''

    vc_sum = stats['vc_sum']
    chart_body += '\n<p>\n  <small>Votantes: ' + str(vc_sum) + '</small>\n  <br>'

    u = stats['u']
    chart_body += '\n  <small>Eleitores: ' + str(u) + '</small>\n</p>'

    absent = stats['absent']
    absent_pc = '{:,.2%}'.format(absent)
    chart_body += '\n<p>\n  <em>Abstenção: ' + absent_pc + '</em>\n</p>'

    stats_markup += render_template('components/pie-chart.html', body=chart_body, pc=((vc_sum / u) * 6.28 * 40), max=(u * 6.28 * 40))

    s = str(stats['s'])
    lead = '\n<p class="lead m-3 text-right">Mandatos: ' + s + '</p>'
    stats_markup += lead

    return stats_markup

def stats_stack(stats, legend = True):
    # v_sum = stats['v_sum']
    vc = '{:,.2%}'.format(stats['vc_sum'] / stats['u'])
    u = '100%'
    absent = '{:,.2%}'.format(stats['absent'])

    # width, style, value, start, max
    stack = [
        (vc, 'primary', vc, 0, u),
        (absent, 'secondary', absent, vc, u)
    ]

    stack_markup = render_template('components/progress-stack.html', items=stack)

    if legend:
        legend_markup = ''
        legend_markup += '<p class="small mb-1 text-primary">Votantes: ' + str(stats['vc_sum']) + '</p>'
        legend_markup += '<p class="small mb-1 text-secondary">Eleitores: ' + str(stats['u']) + '</p>'
        legend_markup += '<p class="font-weight-bold text-secondary">Abstenção: ' + absent + '</p>'
        stack_markup += legend_markup

    return stack_markup

def wasted_stack(stats, waste_sum, legend = True):
    # v_sum = stats['v_sum']
    i = '{:,.2%}'.format(stats['i_sum'] / stats['vc_sum'])
    vc = '100%'
    vs_sum = stats['v_sum'] - waste_sum
    vs = '{:,.2%}'.format(vs_sum / stats['vc_sum'])
    vsi = '{:,.2%}'.format((vs_sum + stats['i_sum'] / stats['vc_sum']))
    waste = '{:,.2%}'.format(waste_sum / stats['vc_sum'])

    # width, style, value, start, max
    stack = [
        (vs, 'success', vs, 0, vc),
        (i, 'warning', i, vs, vc),
        (waste, 'danger', waste, vsi, vc)
    ]

    stack_markup = render_template('components/progress-stack.html', items=stack)

    if legend:
        legend_markup = ''
        legend_markup += '<p class="small mb-1 text-success">Votos que elegem: ' + vs + '</p>'
        legend_markup += '<p class="small mb-1 text-warning">Brancos e nulos: ' + i + '</p>'
        legend_markup += '<p class="font-weight-bold text-danger">Desperdício: ' + waste + '</p>'
        stack_markup += legend_markup

    return stack_markup

def method_stack(df_src, col_pc, dict_meta, title = ''):
    stack_markup = ''
    max = '100%'

    stack = []
    start_raw = 0
    for index, row in df_src.iterrows():
        value_raw = row[col_pc]
        value = '{:.2%}'.format(value_raw)
        if row['Lista'] in dict_meta:
            hex = dict_meta[row['Lista']][1]
        else:
            hex = '#cccccc'
        start = '{:.2%}'.format(start_raw)
        # width, hex, value, start, max
        stack.append((value,hex,value,start,max))
        start_raw += value_raw

    stack_markup += render_template('components/progress-stack-hex.html', items=stack, title=title)

    return stack_markup

def stats_card(title, stats, diff_sum, waste_sum):

    body_markup = ''
    body_markup += '<p class="lead">Mandatos: ' + str(stats['s']) + '</p>'
    body_markup += '<p><strong>Desvios: ' + '{:.2%}'.format(diff_sum) + '</strong></p>'
    body_markup += '<hr />'
    body_markup += wasted_stack(stats, waste_sum)
    body_markup += '<hr />'
    body_markup += stats_stack(stats)

    return render_template('components/card.html', header=title, body=body_markup)

# Interface de linha de comando
# Uso: python components.py

if __name__ == '__main__':

    print('+---------------+')
    print('| components.py |')
    print('+---------------+')

    print('\nContém ferramentas de conversão de dados em componentes de HTML.\n')

    print('\nNenhuma demonstração disponível.\n')
